package org.gecko.selenium;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true)
public class Starter {

	@Reference
	FirefoxDriver driver;

	@Reference
	ConfigurationAdmin configAdmin;

	@Activate
	void activate() throws IOException {

		// Applied wait time
//		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20l));
		// maximize window
		driver.manage().window().maximize();

		// open browser with desried URL
		driver.get("https://www.datainmotion.de");

		driver.findElement(By.id("menu-item-2106")).click();;

		// closing the browser
	//	driver.close();
	}
}

/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.influxdb.impl;

import java.util.HashMap;
import java.util.Map;

import org.gecko.influxdb.api.InfluxDBEntry;

/**
 * 
 * @author ilenia
 * @since May 3, 2019
 */
public class InfluxDBEntryImpl implements InfluxDBEntry {

	private Map<String, Object> fields;
	private Map<String, String> tags;
	private Object measurement;
	private long timestamp;
	
	 /**
	 * Creates a new instance.
	 */
	public InfluxDBEntryImpl() {
		fields = new HashMap<String, Object>();
		tags = new HashMap<String, String>();
		timestamp = 0;
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.queueing.influxdb.api.InfluxdbContext#getFields()
	 */
	@Override
	public Map<String, Object> getFields() {		
		return fields;
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.queueing.influxdb.api.InfluxdbContext#getTags()
	 */
	@Override
	public Map<String, String> getTags() {
		return tags;
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.queueing.influxdb.api.InfluxdbContext#getMeasurement()
	 */
	@Override
	public Object getMeasurement() {
		return measurement;
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.queueing.influxdb.api.InfluxdbContext#setMeasurement(java.lang.Object)
	 */
	@Override
	public void setMeasurement(Object object) {
		this.measurement = object;		
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.queueing.influxdb.api.InfluxdbContext#getTimestamp()
	 */
	@Override
	public long getTimestamp() {
		return timestamp;
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.queueing.influxdb.api.InfluxdbContext#setTimestamp(long)
	 */
	@Override
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp < 0 ? 0 : timestamp;
	}
	
}

/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.influxdb.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This interface provides a way to convert CSV file to a format which can then directly be used into the
 * <code>InfluxDBEntryHelper<code>. 
 * 
 * @author ilenia
 * @since May 14, 2019
 */
public interface CSVReader {
	
	public static final String PROP_HEADER = "header";
	public static final String PROP_SEPARATOR = "separator";
	public static final String PROP_TYPE_ARRAY = "typeDef";
	
	/**
	 * The method to convert CSV file to a format which can then directly be used into the
	 * <code>InfluxDBEntryHelper<code>. 
	 * The first parameter to be provided is the full path (path+name) of the .csv file.
	 * The second parameter is the separator used in the file to separate the different columns ("," for instance).
	 * The third parameter has to be passed as <code>true<code> if the .csv file contains an header with the columns
	 * names. In this case the returning map keys will be the same columns names in the header. 
	 * If set to <code>false<code>, it means the .csv file has no header (the first line is already made of data
	 * to be stored). In this case the returning map keys will be called as "column0", "column1", etc.
	 * 
	 * @param csvFilePath the full path (path+name) of the .csv file
	 * @param separator the separator used in the file to separate the different columns
	 * @param isHeader a boolean to be set to <code>true<code> if the .csv file contains an header, <code>false<code> otherwise
	 * @return A map containing as many keys as the .csv columns and the corresponding lists of points as values.
	 * @throws IOException
	 */
	public Map<String, List<Object>> convert(String csvFilePath, String separator, boolean isHeader) throws IOException;
	
	/**
	 * The same as before but passing an InputStream instead of the file
	 * @param inputStream
	 * @param separator
	 * @param isHeader
	 * @return
	 * @throws IOException
	 */
	public Map<String, List<Object>> convert(InputStream inputStream, String separator, boolean isHeader) throws IOException;
	
	/**
	 * The method to convert CSV file to a format which can then directly be used into the
	 * <code>InfluxDBEntryHelper<code>. 
	 * The first parameter to be provided is the full path (path+name) of the .csv file.
	 * The second parameter is a map with the options. In particular this method has to be preferred with respect to 
	 * <code>convert(String csvFilePath, String separator, boolean isHeader)<code> when the columns of the CSV file do not
	 * contain only String data types. In this case an array of data type could be provided, which corresponds to the data type 
	 * of the different file columns. The String will be parsed as the corresponding data type, if possible, allowing later
	 * to store the data in the InfluxDB with the correct data type.
	 * This is of particular relevance if the user thinks about using math operator when displaying the data (for instance in 
	 * Grafana). No math operation is allowed in the InfluxDB for fields which are saved as String, so if you need to display
	 * an average value, for instance, at some point, you would need to store the data 
	 * 
	 * @param csvFilePath the full path (path+name) of the .csv file
	 * @param separator the separator used in the file to separate the different columns
	 * @param isHeader a boolean to be set to <code>true<code> if the .csv file contains an header, <code>false<code> otherwise
	 * @return A map containing as many keys as the .csv columns and the corresponding lists of points as values.
	 * @throws IOException
	 */
	public Map<String, List<Object>> convert(String csvFilePath, Map<String, Object> convertOptions) throws IOException;
	
	/**
	 * The same as before but passing an InputStream instead of the file
	 * @param inputStream
	 * @param convertOptions
	 * @return
	 * @throws IOException
	 */
	public Map<String, List<Object>> convert(InputStream inputStream, Map<String, Object> convertOptions) throws IOException;
	
	
	
	public static Map<String, Object> createDefaultOptions(String separator, boolean header) {
		Map<String, Object> options = new HashMap<String, Object>();
		options.put(PROP_HEADER, header);
		options.put(PROP_SEPARATOR, separator);
		return options;
	}
}

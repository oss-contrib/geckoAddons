/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.influxdb.api;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.osgi.annotation.versioning.ProviderType;

/**
 * This interface provides a way to interact with the influxdb-java, by providing some useful methods to create, 
 * remove and write data into an InfluxDB. 
 * 
 * @author ilenia
 * @since May 14, 2019
 */
@ProviderType
public interface InfluxDBService{
	
	/**
	 * Creates an InfluxDB with the given name, in case it does not exist.
	 * @param dbName the DB name
	 * @return <code>true<code> if no error occurred, <code>false<code> otherwise
	 */
	public boolean createDB(String dbName);

	/**
	 * Writes to the given DB a single Point defined by the InfluxDBEntry
	 * @param dbName the DB name where we want to write the point
	 * @param entry the InfluxDBEntry which specifies the properties of the point to be written
	 * @return <code>true<code> if no error occurred, <code>false<code> otherwise
	 */
	public boolean writeSinglePoint(String dbName, InfluxDBEntry entry);
	
	/**
	 * Writes to the given DB a whole time series defined by the list of InfluxDBEntry
	 * @param dbName the DB name where we want to write the point
	 * @param entries the list of InfluxDBEntry which specifies the properties of the points to be written
	 * @return <code>true<code> if no error occurred, <code>false<code> otherwise
	 */
	public boolean writeTimeSeries(String dbName, List<InfluxDBEntry> entries);
	
	/**
	 * Drops the DB with the given name
	 * @param dbName the DB name to be removed
	 * @return <code>true<code> if no error occurred, <code>false<code> otherwise
	 */
	public boolean removeDB(String dbName);
	
	/**
	 * InfluxDB does not support time shift series, thus we provide a work-around for this issue, in case the user 
	 * wants to plot two time series, registered at different times, in the same time range. 
	 * The only thing requested from the user is to save an additional tag with the key <code>timeShiftTag<code> 
	 * to the point whose timestamp is to be used later as time shift for other time series. 
	 * @param dbName the DB name to be removed
	 * @param timeShiftTag the tag key under which the time shift has been saved
	 * @return the value of the tag with the time shift, already converted in long
	 */
	public long getTimeShift(String dbName, String timeShiftTag);
	
	
	/**
	 * Allows to write point in the DB, shifting their origin to the value provided by the <code>timeShift<code>
	 * parameter. This is to allow different time series to be plot in the same time range. 
	 * E.g. Time series A has its origin at time <code>timeShift<code>, while time series B has been recorded an hour 
	 * later, but we want to plot them in the same time range. Thus, when saving the points of time series B you should use
	 * this method and provide as 3rd parameter the <code>timeShift<code>, namely the first timestamp of time series A.
	 * The points of time series B will be saved accordingly (the first point of time series B will coincide in time with 
	 * the one of time series A, and the subsequent points will be adjusted accordingly).
	 * 
	 * @param dbName the DB name to be removed
	 * @param entries the list of InfluxDBEntry which specifies the properties of the points to be written
	 * @param timeShift the timestamp to which we want the time series to be adjusted with
	 * @return <code>true<code> if no error occurred, <code>false<code> otherwise
	 */
	public boolean writeWithTimeShift(String dbName, List<InfluxDBEntry> entries, long timeShift);
	
	/**
	 * Returns the list of Pojo objects of class clazz saved in the DB with the specified name 
	 * and under the specified measurement
	 * @param measurement
	 * @param dbName
	 * @param clazz
	 * @return
	 */
	public List<?> getQuery(String measurement, String dbName, Class<?> clazz);
	
	/**
	 * Returns the list of EObject of class eclass saved in the DB with the specified name 
	 * and under the specified measurement
	 * @param measurement the measurement
	 * @param dbName the database name
	 * @param idTag the idtag name
	 * @param idTagValue the value for it
	 * @param eclass the eclass
	 * @param startDate start time
	 * @param endDate end time of the query
	 * @param timeAttribute the optional {@link EAttribute} for the timestamp
	 * @return
	 */
	public List<EObject> getEObjectQuery(String measurement, String dbName, String idTag, String idTagValue, EClass eclass, Long startDate, Long endDate, EAttribute timeAttribute);

	
	/**
	 * Returns a map where the key is the value of the idTag and the value is a list of EObject of class eclass,
	 * which have been retrieved from the influxDB and converted throught the mapper.
	 * To retrieve all the time series within a certain time range ordered according to a certain tag.
	 * 
	 * @param measurement the measurement name
	 * @param dbName the database name
	 * @param idTag the id tag
	 * @param eclass the {@link EClass}
	 * @param startDate the start time
	 * @param endDate the end time
	 * @param timeAttribute the time {@link EAttribute}
	 * @return
	 */
	public Map<String, List<EObject>> getSeriesMap(String measurement, String dbName, String idTag, EClass eclass, Long startDate, Long endDate, EAttribute timeAttribute);
}

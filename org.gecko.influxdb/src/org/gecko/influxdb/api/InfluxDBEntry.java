/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.influxdb.api;

import java.util.Map;

/**
 * This interface allows to set up an InfluxDBEntry to be used to write points in an InfluxDB through the 
 * <code>InfluxDBService<code>.
 * 
 * @author ilenia
 * @since May 3, 2019
 */
public interface InfluxDBEntry {
		
	/**
	 * @return the list of fields for the InfluxDBEntry which will be used to create a Point in the db. 
	 *         At least one filed has to be provided.
	 */
	Map<String, Object> getFields();
	
	
	/**
	 * @return the list of tags for the InfluxDBEntry which will be used to create a Point in the db. 
	 *         The tags are optional.
	 */
	Map<String, String> getTags();
	
	/**
	 * @return the measurement for the InfluxDBEntry which will be used to create a Point in the db. 	 *         
	 */
	Object getMeasurement();
	
	/**
	 * Sets the measurement for the InfluxDBEntry which will be used to create a Point in the db. 	 
	 * This is a mandatory field. Current implementation expects a String.
	 * @param object the measurement to be set
	 */
	void setMeasurement(Object object);
	
	/**
	 * @return the timestamp associated with the measurement in this InfluxDBEntry.
	 */
	long getTimestamp();
	
	/**
	 * Sets the timestamp associated with the measurement in this InfluxDBEntry
	 * The timestamp unit is milliseconds. If no timestamp is provided the Point will be saved in the DB
	 * with the timestamp at the time of the saving operation. 
	 * 
	 * @param timestamp
	 */
	void setTimestamp(long timestamp);
	
}

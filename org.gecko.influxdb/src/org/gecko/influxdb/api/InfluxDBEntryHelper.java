/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.influxdb.api;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This is an Helper interface to provide an easy way to get the InfluxDBEntry list.
 * 
 * @author ilenia
 * @since May 14, 2019
 */
public interface InfluxDBEntryHelper {

	/**
	 * Initialize the InfluxDBEntryHelper with the properties to be set in the InfluxDBEntry. The "tagsKeys" and 
	 * the "timeKey" could be null. In this case no tag will be inserted in the InfluxDBEntry and the timestamp of the
	 * points will be assigned directly once saved in the influxdb, according to the insertion order.
	 * The "measurement" and at least one "filedKey" have to be provided instead.
	 * 
	 * @param measurement
	 * @param fieldKeys
	 * @param tagsKeys
	 * @param timeKey
	 */
	public void initialize(String measurement, Set<String> fieldKeys, Set<String> tagsKeys, String timeKey);
	
	/**
	 * This method returns a list of InfluxDBEntry starting from a Map, in which the keys should be the same used in the 
	 * <code>initialize<code> method, while the values are the actual list of objects to be saved under the corresponding
	 * key.
	 *  
	 * @param map
	 * @return
	 */
	public Set<InfluxDBEntry> getInfluxdbEntries(Map<String, List<Object>> map);
}

/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.influxdb.api;

/**
 * This is just a customer exception for the InfluxDB-associated services
 * 
 * @author ilenia
 * @since May 14, 2019
 */
public class InfluxDBException extends Exception {

	/** serialVersionUID */
	private static final long serialVersionUID = 5653633894121118657L;
	
	/**
	 * Creates a new instance.
	 */
	public InfluxDBException(String errorMessage) {
        super(errorMessage);    
	}
}

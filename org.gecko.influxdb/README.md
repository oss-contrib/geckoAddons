# The InfluxDB DIM API
This project provides an API for the InfluxDB. It allows to create, remove and write data into an InfluxDB. 

## The InfluxDBService
The **InfluxdbService** provides an interface to create, remove and write data into an Influxdb. It makes use of the OSGified version of the **influxdb-java** project. It works in combination with the **InfluxdbEntry**, which is a simple wrapper class to set the properties of an InfluxDB point.

## The InfluxDBEntry
This class provides some methods to set the properties of an InfluxDB point. An InfluxDB point consists of **time** (a timestamp, which, if no provided is set to the timestamp at the time of the insertion in the DB), a **measurement** (“cpu_load”, for example), **at least one key-value field** (the measured value itself, e.g. “value=0.64”, or “temperature=21.2”), and **zero to many key-value tags** containing any metadata about the value (e.g. “host=server01”, “region=EMEA”, “dc=Frankfurt”).  
<br/>
The current implementation supports only type ``String`` for the *measurement*. If no *measurement* or no *field* is provided, or the *measurement* is not of type ``String``, an error is prompt when trying to write data into the DB. 

## The InfluxDBEntryHelper
This is an helper class that provides a way to set up a list of **InfluxDBEntry** to be used later in the **InfluxDBService** to write points into the DB. 

## The CSVReader
This component offers a method which converts CSV files into a Map<String, List<Object>> which can then directly be used in the **InfluxDBEntryHelper** to setup the entries for the **InfluxDBService**.
java: java
javac: javac
javac.source: 1.8
javac.target: 1.8
javac.debug:  on

#Bundle-DocURL: https://yourdoc.de
Bundle-License: Eclipse Public License 1.0
Bundle-Copyright:Data In Motion GmbH all rights reserved
Bundle-Vendor: Data In Motion GmbH
Bundle-ContactAddress: info@data-in-motion.biz
Bundle-Icon: icons/gecko.ico;size=64

#if you want bnd to tell you more during the build
#-verbose: true

# Includes sources from bundle
-sources: true

-includeresource.license: \
	META-INF/LICENSE=${workspace}/cnf/license/LICENSE

-includeresource.icon: \
	icons=${workspace}/cnf/icons
		
-removeheaders: Bnd-LastModified, Tool, Created-By, Private-Package

# This is the version of JUnit that will be used at build time and runtime
geckotest: org.gecko.core.test;version=latest,\
	org.osgi.util.promise;version=latest,\
	org.osgi.util.function;version=latest
# This is the version of JUnit that will be used at build time and runtime
junit: org.apache.servicemix.bundles.junit;version="[4.11,5)"
# This is the version of Mockito that will be used at build time and run time
mockito: org.mockito.mockito-core;version="[1.9,2)",\
  org.objenesis;version="[2.1,3)"

-releaserepo: Release, DIM_Release
-maven-release: pom
-groupid: org.gecko.addons

# Enable semantic versioning for all bundles
-baselinerepo: GeckoAddons
-baseline: *

# Ignore files for baselining
-diffignore: *.xml,\
	*/pom.properties,\
	Require-Capability,\
	Provide-Capability,\
	OSGI-OPT/*
	
# define global blacklist
-runblacklist.default: osgi.identity;filter:='(osgi.identity=osgi.cmpn)'

-testpath: ${junit}

-fixupmessages.metadata: \
    "No metadata for revision"; \
    restrict:=error;\
    is:=warning

-resolve.effective: active
# > Java 8 runtime
#modules: --add-opens=java.base/jdk.internal.loader=ALL-UNNAMED, --add-opens=java.base/java.lang=ALL-UNNAMED, --add-opens=java.base/java.net=ALL-UNNAMED, --add-opens=java.base/java.security=ALL-UNNAMED
#-runvm.default: ${if;${isempty;${system_allow_fail;java --list-modules}};;${modules}}
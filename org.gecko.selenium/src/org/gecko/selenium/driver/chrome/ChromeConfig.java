package org.gecko.selenium.driver.chrome;

import org.gecko.selenium.driver.firefox.ProxyAutoDetect;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriverLogLevel;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition()
@interface ChromeConfig {

	// Service Part
	@AttributeDefinition(description = "path to the executable driver, if null or empty it will use the System Property '"
			+ ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY + "'.")
	String executable();

	@AttributeDefinition(description = "port 0 meany use any free port")
	int port() default 0;

	@AttributeDefinition(description = "timeout in millis")
	int timeout() default 2000;

	@AttributeDefinition(description = "Entry must have the pattern `key=value`")
	String[] environment() default {};

	// ChromeOptions
	ChromeDriverLogLevel logLevel();
	// ChromiumOptons

	String[] arguments();

	String[] encodedExtensions();

	String[] extensions();

	String binary();

	boolean headless() default false;

	// AbstractDriverOptions
	boolean acceptInsecureCerts() default false;

	PageLoadStrategy pageLoadStrategy();

	ProxyAutoDetect proxyAutodetect();

	String proxyFtpProxy();

	String proxyHttpProxy();

	String proxyNoProxy();

	String proxyProxyAutoconfigUrl();

	ProxyType proxyProxyType() default ProxyType.SYSTEM;

	String proxySocksPassword();

	String proxySocksProxy();

	String proxySocksUsername();

	int proxySocksVersion();

	String proxySslProxy();

	boolean strictFileInteractability();

	UnexpectedAlertBehaviour unhandledPromptBehaviour();

	@AttributeDefinition(description = "Entry must have the pattern `key=value`")
	String[] capabilities() default {};

}
package org.gecko.selenium.driver.chrome;

import java.io.File;
import java.time.Duration;
import java.util.Map;
import java.util.stream.Collectors;

import org.gecko.selenium.SeleniumDriver;
import org.gecko.selenium.Util;
import org.gecko.selenium.driver.firefox.ProxyAutoDetect;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.chromium.ChromiumDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.metatype.annotations.Designate;

@SeleniumDriver("chrome")
@Designate(factory = false, ocd = ChromeConfig.class)
@Component(service = { WebDriver.class, RemoteWebDriver.class, ChromiumDriver.class,
		ChromeDriver.class }, scope = ServiceScope.PROTOTYPE)
public class OSGiChromeDriver extends ChromeDriver {

	@Activate
	public OSGiChromeDriver(ChromeConfig config) {
		super(toDriverService(config), toOptions(config));

	}

	private static ChromeOptions toOptions(ChromeConfig config) {
		ChromeOptions options = new ChromeOptions();
		// Chrome
		if (config.logLevel() != null) {
			options.setLogLevel(config.logLevel());
		}
		// Chromium
		if (config.arguments() != null) {
			options.addArguments(config.arguments());
		}
		if (config.encodedExtensions() != null) {
			options.addEncodedExtensions(config.encodedExtensions());
		}
		if (config.extensions() != null) {
			options.addExtensions(Util.toFiles(config.extensions()));
		}
		if (config.binary() != null) {
			options.setBinary(config.binary());
		}
		options.setHeadless(config.headless());
		// AbstractDriverOptions
		options.setAcceptInsecureCerts(config.acceptInsecureCerts());
		if (config.pageLoadStrategy() != null) {
			options.setPageLoadStrategy(config.pageLoadStrategy());
		}
		options.setProxy(toProxy(config));
		options.setStrictFileInteractability(config.strictFileInteractability());
		if (config.unhandledPromptBehaviour() != null) {
			options.setUnhandledPromptBehaviour(config.unhandledPromptBehaviour());
		}
		if (config.binary() != null) {
			options.setBinary(config.binary());
		}
		// Mutable
		Util.toList(config.capabilities()).stream().map(Util::split)
				.forEach(arr -> options.setCapability(arr[0], arr.length == 0 ? null : arr[1]));

		return options;
	}

	private static Proxy toProxy(ChromeConfig config) {
		Proxy proxy = new Proxy();
		if (config.proxyAutodetect() != null) {
			if (config.proxyAutodetect() == ProxyAutoDetect.TRUE) {
				proxy.setAutodetect(true);
			} else {
				proxy.setAutodetect(false);
			}
		}
		if (config.proxyFtpProxy() != null) {
			proxy.setFtpProxy(config.proxyFtpProxy());
		}
		if (config.proxyHttpProxy() != null) {
			proxy.setHttpProxy(config.proxyHttpProxy());
		}
		if (config.proxyNoProxy() != null) {
			proxy.setNoProxy(config.proxyNoProxy());
		}
		if (config.proxyProxyAutoconfigUrl() != null) {
			proxy.setProxyAutoconfigUrl(config.proxyProxyAutoconfigUrl());
		}
		if (config.proxyProxyType() != null) {
			proxy.setProxyType(config.proxyProxyType());
		}
		if (config.proxySocksPassword() != null) {
			proxy.setSocksPassword(config.proxySocksPassword());
		}
		if (config.proxySocksProxy() != null) {
			proxy.setSocksProxy(config.proxySocksProxy());
		}
		if (config.proxySocksUsername() != null) {
			proxy.setSocksUsername(config.proxySocksUsername());
		}
		if (config.proxySocksVersion() != 0) {
			proxy.setSocksVersion(config.proxySocksVersion());
		}
		if (config.proxySslProxy() != null) {
			proxy.setSslProxy(config.proxySslProxy());
		}
		return proxy;
	}

	public static ChromeDriverService toDriverService(ChromeConfig config) {

		Map<String, String> envMap = Util.toList(config.environment()).stream().map(Util::split)
				.collect(Collectors.toMap(arr -> arr[0], arr -> arr.length == 1 ? null : arr[0]));

		ChromeDriverService.Builder builder = new ChromeDriverService.Builder();
		builder = builder.usingPort(config.port());
		builder = builder.withEnvironment(envMap);
		builder = builder.withTimeout(Duration.ofMillis(config.timeout()));

		if (config.executable() != null && !config.executable().isEmpty()) {
			builder = builder.usingDriverExecutable(new File(config.executable()));
		}

		ChromeDriverService service = builder.build();
		return service;

	}
}
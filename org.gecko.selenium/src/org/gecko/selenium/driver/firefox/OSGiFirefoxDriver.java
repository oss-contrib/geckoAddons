package org.gecko.selenium.driver.firefox;

import java.io.File;
import java.time.Duration;
import java.util.Map;
import java.util.stream.Collectors;

import org.gecko.selenium.SeleniumDriver;
import org.gecko.selenium.Util;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.metatype.annotations.Designate;

@SeleniumDriver("firefox")
@Designate(factory = false, ocd = FirefoxConfig.class)
@Component(service = { WebDriver.class, RemoteWebDriver.class, FirefoxDriver.class }, scope = ServiceScope.PROTOTYPE)
public class OSGiFirefoxDriver extends FirefoxDriver {

	@Activate
	public OSGiFirefoxDriver(FirefoxConfig config) {
		super(toDriverService(config), toOptions(config));

	}

	private static FirefoxOptions toOptions(FirefoxConfig config) {
		FirefoxOptions options = new FirefoxOptions();
		// Chrome
		if (config.logLevel() != null) {
			options.setLogLevel(config.logLevel());
		}
		// Chromium
		if (config.arguments() != null) {
			options.addArguments(config.arguments());
		}
//		if (config.encodedExtensions() != null) {
//			options.addEncodedExtensions(config.encodedExtensions());
//		}
//		if (config.extensions() != null) {
//			options.addExtensions(Util.toFiles(config.extensions()));
//		}
		if (config.binary() != null) {
			options.setBinary(config.binary());
		}
		options.setHeadless(config.headless());
		// AbstractDriverOptions
		options.setAcceptInsecureCerts(config.acceptInsecureCerts());
		if (config.pageLoadStrategy() != null) {
			options.setPageLoadStrategy(config.pageLoadStrategy());
		}
		options.setProxy(toProxy(config));
		options.setStrictFileInteractability(config.strictFileInteractability());
		if (config.unhandledPromptBehaviour() != null) {
			options.setUnhandledPromptBehaviour(config.unhandledPromptBehaviour());
		}
		if (config.binary() != null) {
			options.setBinary(config.binary());
		}
		return options;
	}

	private static Proxy toProxy(FirefoxConfig config) {
		Proxy proxy = new Proxy();

		if (config.proxyAutodetect() != null) {
			if (config.proxyAutodetect() == ProxyAutoDetect.TRUE) {
				proxy.setAutodetect(true);
			} else {
				proxy.setAutodetect(false);
			}
		}
		if (config.proxyFtpProxy() != null) {
			proxy.setFtpProxy(config.proxyFtpProxy());
		}
		if (config.proxyHttpProxy() != null) {
			proxy.setHttpProxy(config.proxyHttpProxy());
		}
		if (config.proxyNoProxy() != null) {
			proxy.setNoProxy(config.proxyNoProxy());
		}
		if (config.proxyProxyAutoconfigUrl() != null) {
			proxy.setProxyAutoconfigUrl(config.proxyProxyAutoconfigUrl());
		}
		if (config.proxyProxyType() != null) {
			proxy.setProxyType(config.proxyProxyType());
		}
		if (config.proxySocksPassword() != null) {
			proxy.setSocksPassword(config.proxySocksPassword());
		}
		if (config.proxySocksProxy() != null) {
			proxy.setSocksProxy(config.proxySocksProxy());
		}
		if (config.proxySocksUsername() != null) {
			proxy.setSocksUsername(config.proxySocksUsername());
		}
		if (config.proxySocksVersion() != 0) {
			proxy.setSocksVersion(config.proxySocksVersion());
		}
		if (config.proxySslProxy() != null) {
			proxy.setSslProxy(config.proxySslProxy());
		}
		return proxy;
	}

	public static GeckoDriverService toDriverService(FirefoxConfig config) {

		Map<String, String> envMap = Util.toList(config.environment()).stream().map(Util::split)
				.collect(Collectors.toMap(arr -> arr[0], arr -> arr.length == 1 ? null : arr[0]));

		GeckoDriverService.Builder builder = new GeckoDriverService.Builder();
		builder = builder.usingPort(config.port());
		builder = builder.withEnvironment(envMap);
		builder = builder.withTimeout(Duration.ofMillis(config.timeout()));

		if (config.executable() != null && !config.executable().isEmpty()) {
			builder = builder.usingDriverExecutable(new File(config.executable()));
		}

		GeckoDriverService service = builder.build();
		return service;

	}

}

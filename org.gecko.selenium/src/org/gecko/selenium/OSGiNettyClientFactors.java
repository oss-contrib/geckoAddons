package org.gecko.selenium;

import org.openqa.selenium.remote.http.HttpClient;
import org.openqa.selenium.remote.http.netty.NettyClient;

import aQute.bnd.annotation.spi.ServiceConsumer;
import aQute.bnd.annotation.spi.ServiceProvider;

@ServiceConsumer(value = HttpClient.Factory.class)
@ServiceProvider(HttpClient.Factory.class)
public class OSGiNettyClientFactors extends NettyClient.Factory {
	public OSGiNettyClientFactors() {

	}
}
/**
 * Copyright (c) 2014 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.uriprovider.providers;

import java.io.File;
import java.util.Map;

import org.gecko.util.uriprovider.LocationUriProvider;
import org.gecko.util.uriprovider.exceptions.LocationUriException;

/**
 * Uses the java io temp folder and cleans it up. 
 * The context is the base folder in the temp folder
 * @author Mark Hoffmann
 * @since 02.09.2014
 */
public class TempFolderLocationUriProvider implements LocationUriProvider {
	
	public static final String ID = "id";
	public static final String PATH = "path";
	public static final String FILE = "file";
	public static final String CONTEXT = "context";
	
	protected String id;
	protected String path;
	protected String context;
	protected String file;
	private String tmpPath; 
	private File searchDir;

	/**
	 * Initializes the files
	 */
	public void initialize() {
		path = removeSlash(path);
		String tempDir = System.getProperty("java.io.tmpdir");
		tempDir = tempDir.replace('\\', '/');
		if(tempDir.endsWith("/")){
			tempDir = tempDir.substring(0, tempDir.length() - 1);
		}
		tmpPath = tempDir + "/" + context;
		String searchPath = tmpPath + "/" + path;
		searchDir = new File(searchPath);
		if (!searchDir.exists()) {
			searchDir.mkdirs();
		}
	}
	
	private String removeSlash(String value) {
		return value.replaceAll("/", "").replaceAll("\\\\", "");
	}
	
	/**
	 * Cleans the resources up
	 * @throws Exception
	 */
	public void dispose() throws Exception {
		if (searchDir != null && searchDir.exists()) {
			for (File child : searchDir.listFiles()) {
				if (child.exists()) {
					child.delete();
				}
			}
			searchDir.delete();
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.utilities.uriprovider.LocationUriProvider#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.utilities.uriprovider.LocationUriProvider#getLocationUri()
	 */
	@Override
	public String getLocationUri() {
		String fileExt = "";
		if (file != null) {
			fileExt += "/" + file;
		}
		return "file:///" +  tmpPath + "/" + path + fileExt;
	}
	
	/**
	 * Activate method called by the OSGi framework
	 * @param properties a properties map
	 */
	public void activate(Map<String, String> properties) {
		int propertyCount = 0;
		if (properties.containsKey(ID)) {
			id = properties.get(ID);
			propertyCount++;
		}
		if (properties.containsKey(PATH)) {
			path = properties.get(PATH);
			propertyCount++;
		}
		if (properties.containsKey(CONTEXT)) {
			context = properties.get(CONTEXT);
			propertyCount++;
		}
		if (properties.containsKey(FILE)) {
			file = properties.get(FILE);
			propertyCount++;
		}
		if (propertyCount < 3) {
			throw new LocationUriException("At least one property is missing for the TmpFolderLocationUriProvider");
		}
		initialize();
	}
	
	/**
	 * Deactivate method to clean up all data
	 */
	public void deactivate() {
		try {
			dispose();
		} catch (Exception e) {
			throw new LocationUriException("Error cleaning up TmpFolderLocationUriProvider", e);
		}
	}

}

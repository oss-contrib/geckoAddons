/**
 * Copyright (c) 2014 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.uriprovider.exceptions;

/**
 * Runtime exception for errors with the location URI's
 * @author Mark Hoffmann
 * @since 02.09.2014
 */
public class LocationUriException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public LocationUriException(String message) {
		super(message);
	}
	
	public LocationUriException(String message, Throwable cause) {
		super(message, cause);
	}

}

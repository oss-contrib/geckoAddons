/**
 * Copyright (c) 2014 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.uriprovider;

/**
 * Interface for providing location  URI. Beneath the known schemes, there are additional supported schemes:
 * <li>EWS - Eclipse workspace</li>
 * <li>TMP - OS temp folder</li>
 * </br>
 * To define an Eclipse workspace location provider, the following properties are necessary:
 * <li>ID = <location-provider-id></li>
 * <li>SCHEME = SCHEME_EWS</li>
 * <li>PROJECT = <eclipse-project-name></li>
 * <li>PATH = <location-in-the-project> (optional)</li>
 * <li>FILE = <file-in-the-project> (optional)</li>
 * <li>CONTEXT = <context-information> (optional)</li>
 * </br>
 * The DISPOSE feature is not supported here.
 * </br>
 * To define a temp location provider, the following properties are necessary:
 * <li>ID = <location-provider-id></li>
 * <li>SCHEME = SCHEME_TMP</li>
 * <li>PATH = <location-in-the-tmp-folder> (optional)</li>
 * <li>FILE = <file-in-the-project> (optional)</li>
 * <li>DISPOSE = <TRUE|FALSE> (optional default=FALSE)</li>
 * <li>CONTEXT = <context-information> (optional)</li>
 * </br>
 * To define a file location provider, the following properties are necessary:
 * <li>ID = <location-provider-id></li>
 * <li>SCHEME = SCHEME_FILE</li>
 * <li>LOCATION = <base-location-in-the-filesystem></li>
 * <li>PATH = <location-under-the-base-location> (optional)</li>
 * <li>FILE = <file-in-the-project> (optional)</li>
 * <li>DISPOSE = <TRUE|FALSE> (optional default=FALSE)</li>
 * <li>CONTEXT = <context-information> (optional)</li>
 * </br>
 * To define a mongo location provider, the following properties are necessary:
 * <li>ID = <location-provider-id></li>
 * <li>SCHEME = "MONGODB"</li>
 * <li>LOCATION = <database-hosts-comma-separated></li>
 * <li>PATH = <database-to-connect></li>
 * <li>CONTEXT = <mongo-query> (optional)</li>
 * <li>user = <user-information> (optional)</li>
 * <li>pwd = <password-secret> (optional)</li>
 * @author Mark Hoffmann
 * @since 01.09.2014
 */
public interface LocationUriProvider {
	
	/* Scheme for Eclipse workspace locations */  
	public static final String SCHEME_EWS = "ews";
	/* Scheme for temp folder locations */  
	public static final String SCHEME_TMP = "tmp";
	/* Scheme for file locations */  
	public static final String SCHEME_FILE = "file";
	
	public static final String ID = "id";
	public static final String SCHEME = "scheme";
	public static final String BASE_LOCATION = "location";
	public static final String PATH = "path";
	public static final String CONTEXT = "context";
	/* Only used when scheme is EWS */
	public static final String PROJECT = "project";
	/* Optional to define a file */
	public static final String FILE = "file";
	/* 
	 * Setting this property as Boolean value, indicates to cleanup the files and folders on component de-activation.
	 * This property is not supported for all implementations.
	 */
	public static final String DISPOSE = "dispose";
	
	/**
	 * Returns the location id
	 * @return the location id
	 */
	public String getId();
	
	/**
	 * Returns the location URI as {@link String}
	 * @return the location URI as {@link String}
	 */
	public String getLocationUri();

}

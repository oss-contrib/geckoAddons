/**
 * Copyright (c) 2014 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.uriprovider.mocks;

import org.gecko.util.uriprovider.providers.TempFolderLocationUriProvider;

/**
 * Uses the java io temp folder and cleans it up. 
 * The context is the base folder in the temp folder
 * @author Mark Hoffmann
 * @since 02.09.2014
 */
public class LocationUriProviderMock extends TempFolderLocationUriProvider {
	
	/**
	 * The constructor with id and sub folder path for the data directory, with default context 'test'
	 * @param id the provider id
	 * @param path the sub folder path for the data directory
	 */
	public LocationUriProviderMock(String id, String path) {
		this.id = id;
		this.path = path;
		this.context = "test";
	}
	
	/**
	 * The constructor with id and sub folder path for the data directory
	 * @param id the provider id
	 * @param context the sub folder under the temp directory to store the data into 
	 * @param path the sub folder path for the data directory
	 */
	public LocationUriProviderMock(String id, String context, String path) {
		this.id = id;
		this.context = context;
		this.path = path;
	}
	
	/**
	 * The constructor with id and sub folder path for the data directory
	 * @param id the provider id
	 * @param context the sub folder under the temp directory to store the data into 
	 * @param path the sub folder path for the data directory
	 * @param file the file name
	 */
	public LocationUriProviderMock(String id, String context, String path, String file) {
		this.id = id;
		this.context = context;
		this.path = path;
		this.file = file;
	}
	
}

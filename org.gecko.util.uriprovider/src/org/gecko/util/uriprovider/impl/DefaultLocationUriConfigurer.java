/**
 * Copyright (c) 2012 - 2016 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.uriprovider.impl;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.gecko.util.uriprovider.LocationUriConfigurer;
import org.gecko.util.uriprovider.exceptions.LocationUriException;
import org.gecko.util.uriprovider.providers.DSLocationUriProvider;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * Default implementation for the configurer
 * @author Mark Hoffmann
 * @since 10.11.2016
 */
@Component(name="location_uri_configurer")
public class DefaultLocationUriConfigurer implements LocationUriConfigurer {

	private ConfigurationAdmin configAdmin;

	/* 
	 * (non-Javadoc)
	 * @see de.dim.utilities.uriprovider.LocationUriConfigurer#createLocationUri(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void createLocationUri(String id, String schema, String base, String path, String context) {
		try {
			if (id == null || schema == null || base == null || path == null || context == null) {
				throw new LocationUriException("Error creating uri because of missing arguments");
			}
			Configuration configuration = getConfiguration(id, false);
			if (configuration != null) {
				throw new LocationUriException("There is already an configuration with the given id: " + id);
			}
			configuration = configAdmin.createFactoryConfiguration("uriProviderFactory");
			Dictionary<String, Object> properties = new Hashtable<>();
			properties.put(DSLocationUriProvider.SCHEME, schema);
			properties.put(DSLocationUriProvider.LOCATION, base);
			properties.put(DSLocationUriProvider.PATH, path);
			properties.put(DSLocationUriProvider.CONTEXT, context);
			properties.put(DSLocationUriProvider.ID, id);
			configuration.update(properties);
		} catch (Exception e) {
			if (e instanceof LocationUriException) {
				throw (LocationUriException)e;
			} else {
				throw new LocationUriException("Error creating configuration with PID: " + id);
			}
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.utilities.uriprovider.LocationUriConfigurer#updateLocationUri(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void updateLocationUri(String id, String schema, String base, String path, String context) {
		updateLocationUri(id, schema, base, path, context, false);
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.utilities.uriprovider.LocationUriConfigurer#updateLocationUri(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean)
	 */
	@Override
	public void updateLocationUri(String id, String schema, String base, String path, String context, boolean create) {
		try {
			if (id == null) {
				throw new LocationUriException("Error creating uri because of missing id argument");
			}
			Configuration configuration = getConfiguration(id, create);
			if (configuration == null) {
				return;
			}
			// in case the configuration is new, getProperties will return null
			Dictionary<String, Object> config = configuration.getProperties();;
			Dictionary<String, Object> properties = new Hashtable<>();
			putValue(DSLocationUriProvider.SCHEME, schema, properties, config);
			putValue(DSLocationUriProvider.LOCATION, base, properties, config);
			putValue(DSLocationUriProvider.PATH, path, properties, config);
			putValue(DSLocationUriProvider.CONTEXT, context, properties, config);
			putValue(DSLocationUriProvider.ID, id, properties, config);
			configuration.update(properties);
		} catch (Exception e) {
			if (e instanceof LocationUriException) {
				throw (LocationUriException)e;
			} else {
				throw new LocationUriException("Error creating configuration with PID: " + id);
			}
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see de.dim.utilities.uriprovider.LocationUriConfigurer#deleteLocationUri(java.lang.String)
	 */
	@Override
	public void deleteLocationUri(String id) {
		if (id == null) {
			return;
		}
		try {
			Configuration configuration = getConfiguration(id, false);
			if (configuration != null) {
				configuration.delete();
			}
		} catch (Exception e) {
			throw new LocationUriException("Error deleting configuration with PID: " + id, e);
		}
	}
	
	/**
	 * Sets the {@link ConfigurationAdmin} reference via OSGi-DS
	 * @param configAdmin the {@link ConfigurationAdmin} instance to set 
	 */
	@Reference(cardinality=ReferenceCardinality.MANDATORY, policy=ReferencePolicy.STATIC, unbind="unsetConfigAdmin")
	public void setConfigAdmin(ConfigurationAdmin configAdmin) {
		this.configAdmin = configAdmin;
	}
	
	/**
	 * Un-sets the {@link ConfigurationAdmin} reference via OSGi-DS
	 * @param configAdmin the {@link ConfigurationAdmin} instance to unset
	 */
	public void unsetConfigAdmin(ConfigurationAdmin configAdmin) {
		this.configAdmin = configAdmin;
	}
	
	/**
	 * Returns the configuration with the given id or <code>null</code>, if nothing was found or parameter is <code>null</code>
	 * @param configurationId the configuration id
	 * @param create set to true, to create a new configuration if no one exists
	 * @return the configuration with the given id or <code>null</code>, if nothing was found 
	 */
	private Configuration getConfiguration(String configurationId, boolean create) {
		if (configurationId == null) {
			return null;
		}
		Configuration[] configurations;
		try {
			configurations = configAdmin.listConfigurations("(&(service.factoryPid=uriProviderFactory)(id=" + configurationId + "))");
			Configuration configuration = null;
			if (configurations != null && configurations.length > 0) {
				configuration = configurations[0];
			} else {
				if (create) {
					configuration = configAdmin.createFactoryConfiguration("uriProviderFactory");
				}
			}
			return configuration;
		} catch (IOException e) {
			throw new LocationUriException("Error getting configuration with PID: " + configurationId, e);
		} catch (InvalidSyntaxException e) {
			throw new LocationUriException("Error evaluating filter: (&(service.factoryPid=uriProviderFactory)(id=" + configurationId + "))", e);
		}
	}
	
	/**
	 * Checks if the value is set and put itto the properties map
	 * @param key the key
	 * @param value the value
	 * @param properties the properties map
	 * @param config the existing configuration, can be <code>null</code>
	 */
	private void putValue(String key, String value, Dictionary<String, Object> properties, Dictionary<String, Object> config) {
		if (key == null || properties == null) {
			return;
		}
		if (value != null) {
			properties.put(key, value);
		} else {
			if (config != null) {
				properties.put(key, config.get(key));
			}
		}
	}

}

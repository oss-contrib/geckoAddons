/**
 * Copyright (c) 2012 - 2016 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.uriprovider;

/**
 * Configuration service for the creation, update and delete of the {@link LocationUriProvider} using
 * configuration admin
 * @author Mark Hoffmann
 * @since 10.11.2016
 */
public interface LocationUriConfigurer {
	
	/**
	 * Creates a new configuration for a provider for the given parameters
	 * @param id the provider id/configuration id
	 * @param schema the uri schema
	 * @param base a base uri
	 * @param path a path under the base uri
	 * @param context a special context identifier, which is a last segment in the uri path
	 */
	public void createLocationUri(String id, String schema, String base, String path, String context);
	
	/**
	 * Updates a new configuration for a provider for the given parameters. If no configuration exists this
	 * method does nothing. It is the same like calling the second method with parameter 'create=false'
	 * @param id the provider id/configuration id
	 * @param schema the uri schema
	 * @param base a base uri
	 * @param path a path under the base uri
	 * @param context a special context identifier, which is a last segment in the uri path
	 */
	public void updateLocationUri(String id, String schema, String base, String path, String context);
	
	/**
	 * Updates a new configuration for a provider for the given parameters. 
	 * @param id the provider id/configuration id
	 * @param schema the uri schema, can be <code>null</code>
	 * @param base a base uri, can be <code>null</code>
	 * @param path a path under the base uri, can be <code>null</code>
	 * @param context a special context identifier, which is a last segment in the uri path, can be <code>null</code>
	 * @param create set to <code>true</code>, to create a new configuration, if none already exists
	 */
	public void updateLocationUri(String id, String schema, String base, String path, String context, boolean create);
	
	/**
	 * Removes a configuration with the given id
	 * @param id the configuration id
	 */
	public void deleteLocationUri(String id);
	
}

package org.gecko.util.uriprovider.command;

import org.gecko.util.uriprovider.LocationUriConfigurer;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * Command to create, update or delete Uri providers
 * @author Mark Hoffmann
 * @since 08.11.2016
 */
@Component(service={UriProviderCommand.class}, name="uri", property = {
		"osgi.command.scope=uri", 
		"osgi.command.dunction=help", 
		"osgi.command.dunction=updateUri", 
		"osgi.command.dunction=deleteUri", 
		"osgi.command.dunction=createUri" 
		})
public class UriProviderCommand {
	
	private LocationUriConfigurer configurer;

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.framework.console.CommandProvider#getHelp()
	 */
	public void help() {
		String lb = System.getProperty("line.separator");
		StringBuilder sb  = new StringBuilder();
		sb.append("---DIM URI Provider Commands---");
		sb.append(lb);
		sb.append(getCreateHelp());
		sb.append(lb);
		sb.append(getUpdateHelp());
		sb.append(lb);
		sb.append(getDeleteHelp());
		sb.append(lb);
		System.out.println(sb.toString());
	}
	
	public void createUri(String schema, String location, String path, String context, String id) {
		try {
			configurer.createLocationUri(id, schema, location, path, context);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace(System.err);
		}
	}
	
	public void updateUri(String id, String schema, String location, String path, String context) {
		
		try {
			configurer.updateLocationUri(id, schema, location, path, context, false);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace(System.err);
		}
	}
	
	public void deleteUri(String id) {
		try {
			configurer.deleteLocationUri(id);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace(System.err);
		}
	}
	
	/**
	 * Sets the {@link LocationUriConfigurer} reference via OSGi-DS
	 * @param configurer the {@link LocationUriConfigurer} instance to set 
	 */
	@Reference(cardinality=ReferenceCardinality.MANDATORY, policy=ReferencePolicy.STATIC)
	public void setConfigurer(LocationUriConfigurer configurer) {
		this.configurer = configurer;
	}
	
	/**
	 * Un-sets the {@link LocationUriConfigurer} reference via OSGi-DS
	 * @param configurer the {@link LocationUriConfigurer} instance to unset
	 */
	public void unsetConfigurer(LocationUriConfigurer configurer) {
		this.configurer = configurer;
	}
	
	/**
	 * Creates the help {@link String} for the createUri command 
	 * @return the help {@link String} 
	 */
	private String getCreateHelp() {
		String lb = System.getProperty("line.separator");
		StringBuilder sb  = new StringBuilder();
		sb.append("createUri - Creates an uri provider");
		sb.append(lb);
		sb.append("  parameters:");
		sb.append(lb);
		sb.append("    scheme		the uri scheme, e.g. file, http");
		sb.append(lb);
		sb.append("    location		the base location, e.g. /home/test");
		sb.append(lb);
		sb.append("    path        	the path in the base folder, e.g. search");
		sb.append(lb);
		sb.append("    context      the context, e.g. facet, idx");
		sb.append(lb);
		sb.append("    id      		the optional id of the provider");
		return sb.toString();
	}
	
	/**
	 * Creates the help {@link String} for the updateUri command 
	 * @return the help {@link String} 
	 */
	private String getUpdateHelp() {
		String lb = System.getProperty("line.separator");
		StringBuilder sb  = new StringBuilder();
		sb.append("updateUri - Updates an uri provider");
		sb.append(lb);
		sb.append("  parameters: id (<parameter>=<value>)");
		sb.append(lb);
		sb.append("    id      		the id of the provider");
		sb.append(lb);
		sb.append("    scheme =		the uri scheme, e.g. file, http");
		sb.append(lb);
		sb.append("    location	=	the base location, e.g. /home/test");
		sb.append(lb);
		sb.append("    path =       the path in the base folder, e.g. search");
		sb.append(lb);
		sb.append("    context =    the context, e.g. facet, idx");
		sb.append(lb);
		return sb.toString();
	}
	
	/**
	 * Creates the help {@link String} for the updateUri command 
	 * @return the help {@link String} 
	 */
	private String getDeleteHelp() {
		String lb = System.getProperty("line.separator");
		StringBuilder sb  = new StringBuilder();
		sb.append("deleteUri - Deletes an uri provider");
		sb.append(lb);
		sb.append("  parameters:");
		sb.append(lb);
		sb.append("    id      		the id of the provider");
		return sb.toString();
	}
	
}

# Content

* Gecko EMF JSON and YAML support
* EMF Message body writer for JaxRs Whiteboard
* Pac4J Security Feature for JaxRs Whiteboard

# Repositories

OBR: [http://devel.data-in-motion.biz/public/repository/gecko/release/geckoAddons/](http://devel.data-in-motion.biz/public/repository/gecko/release/geckoAddons/)
Nexus: [http://devel.data-in-motion.biz/nexus](http://devel.data-in-motion.biz/nexus/#browse/browse/components:maven-releases)

## Dependencies:

[GeckoREST](http://devel.data-in-motion.biz/public/repository/gecko/release/geckoREST/)
[GeckoEMF](http://devel.data-in-motion.biz/public/repository/gecko/release/geckoEMF/)
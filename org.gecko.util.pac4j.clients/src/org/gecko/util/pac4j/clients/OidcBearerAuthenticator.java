package org.gecko.util.pac4j.clients;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.pac4j.core.context.WebContext;
import org.pac4j.core.credentials.authenticator.Authenticator;
import org.pac4j.core.exception.TechnicalException;
import org.pac4j.core.util.CommonHelper;
import org.pac4j.oidc.config.OidcConfiguration;
import org.pac4j.oidc.credentials.OidcCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.auth.ClientAuthentication;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.auth.ClientSecretPost;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.sdk.UserInfoErrorResponse;
import com.nimbusds.openid.connect.sdk.UserInfoRequest;
import com.nimbusds.openid.connect.sdk.UserInfoResponse;
import com.nimbusds.openid.connect.sdk.UserInfoSuccessResponse;

/**
 * The OpenID Connect authenticator.
 *
 * @author Jerome Leleu
 * @since 1.9.2
 */
public class OidcBearerAuthenticator implements Authenticator<OidcCredentials> {

    private static final Logger logger = LoggerFactory.getLogger(OidcBearerAuthenticator.class);

    private static final Collection<ClientAuthenticationMethod> SUPPORTED_METHODS = 
            Arrays.asList(
                    ClientAuthenticationMethod.CLIENT_SECRET_POST, 
                    ClientAuthenticationMethod.CLIENT_SECRET_BASIC);

    protected OidcConfiguration configuration;

//    protected OidcClient client;

    private ClientAuthentication clientAuthentication;

    public OidcBearerAuthenticator(final OidcConfiguration configuration) {
        CommonHelper.assertNotNull("configuration", configuration);
        this.configuration = configuration;

        // check authentication methods
        final List<ClientAuthenticationMethod> metadataMethods = configuration.findProviderMetadata().getTokenEndpointAuthMethods();

        final ClientAuthenticationMethod preferredMethod = getPreferredAuthenticationMethod(configuration); 
        
        final ClientAuthenticationMethod chosenMethod;
        if (CommonHelper.isNotEmpty(metadataMethods)) {
            if (preferredMethod != null) {
                if (metadataMethods.contains(preferredMethod)) {
                    chosenMethod = preferredMethod;
                } else {
                    throw new TechnicalException(
                        "Preferred authentication method (" + preferredMethod + ") not supported " + 
                        "by provider according to provider metadata (" + metadataMethods + ").");
                }
            } else {
                chosenMethod = firstSupportedMethod(metadataMethods);
            }
        } else {
            chosenMethod = preferredMethod != null ? preferredMethod : ClientAuthenticationMethod.getDefault();
            logger.info("Provider metadata does not provide Token endpoint authentication methods. Using: {}",
                    chosenMethod);
        }

        final ClientID _clientID = new ClientID(configuration.getClientId());
        final Secret _secret = new Secret(configuration.getSecret());
        if (ClientAuthenticationMethod.CLIENT_SECRET_POST.equals(chosenMethod)) {
            clientAuthentication = new ClientSecretPost(_clientID, _secret);
        } else if (ClientAuthenticationMethod.CLIENT_SECRET_BASIC.equals(chosenMethod)) {
            clientAuthentication = new ClientSecretBasic(_clientID, _secret);
        } else {
            throw new TechnicalException("Unsupported client authentication method: " + chosenMethod);
        }
    }

    /**
     * The preferred {@link ClientAuthenticationMethod} specified in the given
     * {@link OidcConfiguration}, or <code>null</code> meaning that the a
     * provider-supported method should be chosen.
     */
    private static ClientAuthenticationMethod getPreferredAuthenticationMethod(OidcConfiguration config) {
        final ClientAuthenticationMethod configurationMethod = config.getClientAuthenticationMethod();
        if (configurationMethod == null) {
            return null;
        }
        
        if (!SUPPORTED_METHODS.contains(configurationMethod)) {
            throw new TechnicalException("Configured authentication method (" + configurationMethod + ") is not supported.");
        }
        
        return configurationMethod;
    }

    /**
     * The first {@link ClientAuthenticationMethod} from the given list of
     * methods that is supported by this implementation.
     * 
     * @throws TechnicalException
     *         if none of the provider-supported methods is supported.
     */
    private static ClientAuthenticationMethod firstSupportedMethod(final List<ClientAuthenticationMethod> metadataMethods) {
        Optional<ClientAuthenticationMethod> firstSupported = 
            metadataMethods.stream().filter((m) -> SUPPORTED_METHODS.contains(m)).findFirst();
        if (firstSupported.isPresent()) {
            return firstSupported.get();
        } else {
            throw new TechnicalException("None of the Token endpoint provider metadata authentication methods are supported: " + 
                metadataMethods);
        }
    }

    @Override
    public void validate(final OidcCredentials credentials, final WebContext context) {
        final AuthorizationCode code = credentials.getCode();
        // if we have a code
        if (code != null) {
            try {
                // Token request
            	UserInfoRequest request = new UserInfoRequest(configuration.findProviderMetadata().getUserInfoEndpointURI(), new BearerAccessToken(credentials.getAccessToken().getValue()));
                HTTPRequest tokenHttpRequest = request.toHTTPRequest();
                tokenHttpRequest.setConnectTimeout(configuration.getConnectTimeout());
                tokenHttpRequest.setReadTimeout(configuration.getReadTimeout());

                logger.info("Trying acces on {} with headers {} and query {}",tokenHttpRequest.getURL().toString(),  tokenHttpRequest.getHeaders(), tokenHttpRequest.getQuery());
                
                
                final HTTPResponse httpResponse = tokenHttpRequest.send();
                logger.debug("Token response: status={}, content={}", httpResponse.getStatusCode(),
                        httpResponse.getContent());

                final UserInfoResponse response = UserInfoResponse.parse(httpResponse);
                if (response instanceof UserInfoErrorResponse) {
                    throw new TechnicalException("Bad token response, error=" + ((UserInfoErrorResponse) response).getErrorObject());
                }
                logger.debug("Token response successful");
                final UserInfoSuccessResponse successResponse = (UserInfoSuccessResponse) response;

                // save tokens in credentials
                credentials.setIdToken(successResponse.getUserInfoJWT());

            } catch (final IOException | ParseException e) {
                throw new TechnicalException(e);
            }
        }
    }

    public ClientAuthentication getClientAuthentication() {
        return clientAuthentication;
    }

    public void setClientAuthentication(final ClientAuthentication clientAuthentication) {
        this.clientAuthentication = clientAuthentication;
    }
}

# INFLUX DB
InfluxDB is a time series database. A time series database (TSDB) is a database optimized for time-stamped or time series data. Time series data are simply measurements or events that are tracked, monitored, downsampled, and aggregated over time. This could be server metrics, application performance monitoring, network data, sensor data, events, clicks, trades in a market, and many other types of analytics data.

## Some tips on InfluxDB
* Installation instructions for the InfluxDB are available [here](https://docs.influxdata.com/influxdb/v1.7/introduction/installation).  
<br/>  
* The default configuration of the InfluxDB makes the service run on **port 8086**. To configure it at a different port, you would need to modify the configuration file located in ``/etc/influxdb/influxdb.conf`` for default installations.  
<br/>
* Once installed InfluxDB and started the corresponding service, you should be able to access the **command line interface** by simply typing ``influx`` on the command line. It uses Influx Query Language (a.k.a InfluxQL) statements. More tips on the syntax [here](https://docs.influxdata.com/influxdb/v1.7/introduction/getting-started/).  
<br/>
* An InfluxDB point consists of **time** (a timestamp, which, if no provided is set to the timestamp at the time of the insertion in the DB), a **measurement** (“cpu_load”, for example), **at least one key-value field** (the measured value itself, e.g. “value=0.64”, or “temperature=21.2”), and **zero to many key-value tags** containing any metadata about the value (e.g. “host=server01”, “region=EMEA”, “dc=Frankfurt”).  
<br/>
* Conceptually you can think of a measurement as an SQL table, where **the primary index is always time**. If two points have the same tags and the same timestamp, no matter whether the field value is different, only the last one will be written into the DB. **To allow storage of points with the same timestamp, you should take care of setting the tags to different values between the two points you want to insert. In such a way both points are properly stored.**

## InfluxDB-java
This project contains an OSGified re-package of the influxdb-java project (available [here](https://github.com/influxdata/influxdb-java)). This implementation is meant as a Java rewrite of the influxdb-go package. All low level REST Api calls are available. 


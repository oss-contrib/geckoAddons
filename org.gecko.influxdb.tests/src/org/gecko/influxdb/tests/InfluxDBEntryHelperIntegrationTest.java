/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.influxdb.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.influxdb.api.CSVReader;
import org.gecko.influxdb.api.InfluxDBEntry;
import org.gecko.influxdb.api.InfluxDBEntryHelper;
import org.gecko.influxdb.api.InfluxDBService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.cm.Configuration;

/**
 * Integration Tests for the InfluxDBEntryHelper
 * 
 * @author ilenia
 * @since May 6, 2019
 */
@RunWith(MockitoJUnitRunner.class)
public class InfluxDBEntryHelperIntegrationTest extends AbstractOSGiTest {
	
	/**
	 * Creates a new instance.
	 * @param bundleContext
	 */
	public InfluxDBEntryHelperIntegrationTest() {
		super(FrameworkUtil.getBundle(InfluxDBEntryHelperIntegrationTest.class).getBundleContext());
	}

	/**
	 * Here you can put everything you want to be exectued before every test
	 */
	public void doBefore() {
		
	}
	
	/**
	 * Here you can put everything you want to be exectued after every test
	 */
	public void doAfter() {
		
	}
	
	/**
	 * Tests the activation of the InfluxDBEntryHelper
	 */
	@Test
	public void testInfluxdbEntryHelperCreation() {
		
		ServiceChecker<InfluxDBEntryHelper> checker = createStaticTrackedChecker(InfluxDBEntryHelper.class);
		checker.assertCreations(1, true);
		
		InfluxDBEntryHelper service = checker.getTrackedService();
		assertNotNull(service);		
	}
	
	
	/**
	 * Tests the method for the setup of the InfluxDBEntry in the InfluxDBEntryHelper. 
	 * Use the CSVReader to create the input map and the InfluxDBService to save it in the db.
	 * @throws IOException 
	 */
//	@Test
	public void testInfluxdbEntryHelperOK() throws IOException {
		
		ServiceChecker<CSVReader> readerChecker = createStaticTrackedChecker(CSVReader.class);
		readerChecker.assertCreations(1, true);
		
		CSVReader reader = readerChecker.getTrackedService();
		assertNotNull(reader);
		
		String filePath = "data/2019-05-12T13-31-04.csv";
		Map<String, List<Object>> result = reader.convert(filePath, ",", true);
		assertTrue(result.containsKey("timestamp"));
		assertTrue(result.containsKey("speed in km/h"));
		assertTrue(result.containsKey("latitude"));
		assertTrue(result.containsKey("longitude"));
		
		assertEquals("1557660680836", result.get("timestamp").get(0));
		assertEquals("0", result.get("speed in km/h").get(0));
		assertEquals("50.858355349700865", result.get("latitude").get(0));
		assertEquals("12.162946570149", result.get("longitude").get(0));
		
		ServiceChecker<InfluxDBEntryHelper> checker = createStaticTrackedChecker(InfluxDBEntryHelper.class);
		checker.assertCreations(1, true);
		
		InfluxDBEntryHelper service = checker.getTrackedService();
		assertNotNull(service);		
		
		Set<String> fieldKeys = new HashSet<String>();
		fieldKeys.add("speed in km/h");
	
		Set<String> tagsKeys = new HashSet<String>();
		tagsKeys.add("latitude");
		tagsKeys.add("longitude");
		
		String timeKey = "timestamp";
		
		service.initialize("traffic", fieldKeys, tagsKeys, timeKey);
		Set<InfluxDBEntry> entries = service.getInfluxdbEntries(result);
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("protocol", "https");
		properties.put("hostname", "devel.data-in-motion.biz");
		properties.put("port", "8286");
		properties.put("username", "influxdb_admin");
		properties.put("password", "influxdb_admin_password");
		
		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
		ServiceChecker<?> influxChecker = getServiceCheckerForConfiguration(configuration);
		influxChecker.assertCreations(1, true);
		
		InfluxDBService influxService = getService(InfluxDBService.class);
		assertNotNull(influxService);
		
		assertTrue(influxService.createDB("test"));
		assertTrue(influxService.writeTimeSeries("test", new ArrayList<>(entries)));
		assertTrue(influxService.removeDB("test"));
	}
	
	/**
	 * If the "measurement" parameter is set to null, no InfluxDBEntry will be set.
	 * @throws IOException 
	 */
	@Test
	public void testWithNoMeasurement() throws IOException {

		ServiceChecker<CSVReader> readerChecker = createStaticTrackedChecker(CSVReader.class);
		readerChecker.assertCreations(1, true);
		
		CSVReader reader = readerChecker.getTrackedService();
		assertNotNull(reader);
		
		String filePath = "data/2019-05-12T13-31-04.csv";
		Map<String, List<Object>> result = reader.convert(filePath, ",", true);
		assertTrue(result.containsKey("timestamp"));
		assertTrue(result.containsKey("speed in km/h"));
		assertTrue(result.containsKey("latitude"));
		assertTrue(result.containsKey("longitude"));
		
		assertEquals("1557660680836", result.get("timestamp").get(0));
		assertEquals("0", result.get("speed in km/h").get(0));
		assertEquals("50.858355349700865", result.get("latitude").get(0));
		assertEquals("12.162946570149", result.get("longitude").get(0));
		
		ServiceChecker<InfluxDBEntryHelper> checker = createStaticTrackedChecker(InfluxDBEntryHelper.class);
		checker.assertCreations(1, true);
		
		InfluxDBEntryHelper service = checker.getTrackedService();
		assertNotNull(service);		
		
		Set<String> fieldKeys = new HashSet<String>();
		fieldKeys.add("speed in km/h");
	
		Set<String> tagsKeys = new HashSet<String>();
		tagsKeys.add("latitude");
		tagsKeys.add("longitude");
		
		String timeKey = "timestamp";
		
		service.initialize(null, fieldKeys, tagsKeys, timeKey);
		Set<InfluxDBEntry> entries = service.getInfluxdbEntries(result);
		assertTrue(entries.isEmpty());
	}
	
	/**
	 * If no fields keys are passed, no InfluxDBEntry will be set.
	 * @throws IOException 
	 */
	@Test
	public void testNoFields() throws IOException {

		ServiceChecker<CSVReader> readerChecker = createStaticTrackedChecker(CSVReader.class);
		readerChecker.assertCreations(1, true);
		
		CSVReader reader = readerChecker.getTrackedService();
		assertNotNull(reader);
		
		String filePath = "data/2019-05-12T13-31-04.csv";
		Map<String, List<Object>> result = reader.convert(filePath, ",", true);
		assertTrue(result.containsKey("timestamp"));
		assertTrue(result.containsKey("speed in km/h"));
		assertTrue(result.containsKey("latitude"));
		assertTrue(result.containsKey("longitude"));
		
		assertEquals("1557660680836", result.get("timestamp").get(0));
		assertEquals("0", result.get("speed in km/h").get(0));
		assertEquals("50.858355349700865", result.get("latitude").get(0));
		assertEquals("12.162946570149", result.get("longitude").get(0));
		
		ServiceChecker<InfluxDBEntryHelper> checker = createStaticTrackedChecker(InfluxDBEntryHelper.class);
		checker.assertCreations(1, true);
		
		InfluxDBEntryHelper service = checker.getTrackedService();
		assertNotNull(service);		
		
		Set<String> fieldKeys = new HashSet<String>();
		fieldKeys.add("speed in km/h");
	
		Set<String> tagsKeys = new HashSet<String>();
		tagsKeys.add("latitude");
		tagsKeys.add("longitude");
		
		String timeKey = "timestamp";
		
		service.initialize("traffic", null, tagsKeys, timeKey);
		Set<InfluxDBEntry> entries = service.getInfluxdbEntries(result);
		assertTrue(entries.isEmpty());
	}
	
	/**
	 * If a tag key is passed but this does not match any keys in the passed map, it will be removed from the tags to be
	 * set in the InfluxDBEntry and will not be set. Since tags are optional for InfluxDB the InfluxDBEntrty will still
	 * be created, just without the wrong tag.
	 * @throws IOException 
	 */
	@Test
	public void testWrongTag() throws IOException {

		ServiceChecker<CSVReader> readerChecker = createStaticTrackedChecker(CSVReader.class);
		readerChecker.assertCreations(1, true);
		
		CSVReader reader = readerChecker.getTrackedService();
		assertNotNull(reader);
		
		String filePath = "data/2019-05-12T13-31-04.csv";
		Map<String, List<Object>> result = reader.convert(filePath, ",", true);
		assertTrue(result.containsKey("timestamp"));
		assertTrue(result.containsKey("speed in km/h"));
		assertTrue(result.containsKey("latitude"));
		assertTrue(result.containsKey("longitude"));
		
		assertEquals("1557660680836", result.get("timestamp").get(0));
		assertEquals("0", result.get("speed in km/h").get(0));
		assertEquals("50.858355349700865", result.get("latitude").get(0));
		assertEquals("12.162946570149", result.get("longitude").get(0));
		
		ServiceChecker<InfluxDBEntryHelper> checker = createStaticTrackedChecker(InfluxDBEntryHelper.class);
		checker.assertCreations(1, true);
		
		InfluxDBEntryHelper service = checker.getTrackedService();
		assertNotNull(service);		
		
		Set<String> fieldKeys = new HashSet<String>();
		fieldKeys.add("speed in km/h");
	
		Set<String> tagsKeys = new HashSet<String>();
		tagsKeys.add("latitude");
		tagsKeys.add("longitudeNO");
		
		String timeKey = "timestamp";
		
		service.initialize("traffic", fieldKeys, tagsKeys, timeKey);
		Set<InfluxDBEntry> entries = service.getInfluxdbEntries(result);
		for(InfluxDBEntry entry : entries) {
			assertFalse(entry.getTags().containsKey("longitudeNO"));
			assertFalse(entry.getTags().containsKey("longitude"));
		}		
	}
	

	/**
	 * To insert the traffic data from Mark app.
	 * @throws IOException
	 */
//	@Test
	public void testInsertSmartCityData() throws IOException {
		

		ServiceChecker<CSVReader> readerChecker = createStaticTrackedChecker(CSVReader.class);
		readerChecker.assertCreations(1, true);
		
		CSVReader reader = readerChecker.getTrackedService();
		assertNotNull(reader);
		
		String filePath = "/home/ilenia/Documents/smartCity_project/csv-influxdb/2019-05-16T08-46-17.csv";
		Map<String, Object> options = CSVReader.createDefaultOptions(",", true);
		Class<?>[] types = {Long.TYPE, Integer.TYPE, String.class, String.class, String.class, String.class};
		options.put(CSVReader.PROP_TYPE_ARRAY, types);
		
		Map<String, List<Object>> result = reader.convert(filePath, options);
		assertTrue(result.containsKey("time"));
		assertTrue(result.containsKey("speed"));
		assertTrue(result.containsKey("lat"));
		assertTrue(result.containsKey("lng"));		
		assertTrue(result.containsKey("id"));		
		assertTrue(result.containsKey("tag"));		
		
		ServiceChecker<InfluxDBEntryHelper> checker = createStaticTrackedChecker(InfluxDBEntryHelper.class);
		checker.assertCreations(1, true);
		
		InfluxDBEntryHelper service = checker.getTrackedService();
		assertNotNull(service);		
		
		Set<String> fieldKeys = new HashSet<String>();
		fieldKeys.add("speed");
	
		Set<String> tagsKeys = new HashSet<String>();
		tagsKeys.add("lat");
		tagsKeys.add("lng");
		tagsKeys.add("id");
		tagsKeys.add("tag");
		
		String timeKey = "time";
		
		service.initialize("traffic", fieldKeys, tagsKeys, timeKey);
		Set<InfluxDBEntry> entries = service.getInfluxdbEntries(result);
		
		for(InfluxDBEntry entry : entries) {
			entry.getTags().put("LOC", "Gera-Jena");
		}
		
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("protocol", "https");
		properties.put("hostname", "devel.data-in-motion.biz");
		properties.put("port", "8286");
		properties.put("username", "influxdb_admin");
		properties.put("password", "influxdb_admin_password");
//		properties.put("protocol", "http");
//		properties.put("hostname", "localhost");
//		properties.put("port", "8086");
//		properties.put("username", "root");
//		properties.put("password", "root");
		
		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
		ServiceChecker<?> influxChecker = getServiceCheckerForConfiguration(configuration);
		influxChecker.assertCreations(1, true);
		
		InfluxDBService influxService = getService(InfluxDBService.class);
		assertNotNull(influxService);
		
		assertTrue(influxService.createDB("test"));
		assertTrue(influxService.writeTimeSeries("test", new ArrayList<>(entries)));
	}
	
}

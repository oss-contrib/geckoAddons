/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.influxdb.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.influxdb.api.CSVReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;

/**
 * Integration Tests for the CSVReader
 * 
 * @author ilenia
 * @since May 6, 2019
 */
@RunWith(MockitoJUnitRunner.class)
public class CSVReaderIntegrationTest extends AbstractOSGiTest {
	
	/**
	 * Creates a new instance.
	 * @param bundleContext
	 */
	public CSVReaderIntegrationTest() {
		super(FrameworkUtil.getBundle(CSVReaderIntegrationTest.class).getBundleContext());
	}

	/**
	 * Here you can put everything you want to be exectued before every test
	 */
	public void doBefore() {
		
	}
	
	/**
	 * Here you can put everything you want to be exectued after every test
	 */
	public void doAfter() {
		
	}
	
	/**
	 * Tests the activation of the CSVReader
	 */
	@Test
	public void testCSVReaderCreation() {
		
		ServiceChecker<CSVReader> checker = createStaticTrackedChecker(CSVReader.class);
		checker.assertCreations(1, true);
		
		CSVReader service = checker.getTrackedService();
		assertNotNull(service);		
	}
	
	
	/**
	 * Tests the method for the conversion of a CSV file into a map of the CSVReader, in the case that the CSV file
	 * has a header. In this case the keys of the returning map will be the name of the columns in the CSV file.
	 * @throws IOException 
	 */
	@Test
	public void testCSVReaderConvertHeader() throws IOException {
		
		ServiceChecker<CSVReader> checker = createStaticTrackedChecker(CSVReader.class);
		checker.assertCreations(1, true);
		
		CSVReader service = checker.getTrackedService();
		assertNotNull(service);
		
		String filePath = "data/2019-05-12T13-31-04.csv";
		Map<String, List<Object>> result = service.convert(filePath, ",", true);
		assertTrue(result.containsKey("timestamp"));
		assertTrue(result.containsKey("speed in km/h"));
		assertTrue(result.containsKey("latitude"));
		assertTrue(result.containsKey("longitude"));
		
		assertEquals("1557660680836", result.get("timestamp").get(0));
		assertEquals("0", result.get("speed in km/h").get(0));
		assertEquals("50.858355349700865", result.get("latitude").get(0));
		assertEquals("12.162946570149", result.get("longitude").get(0));
	}
	
	/**
	 * Tests the method for the conversion of a CSV file into a map of the CSVReader, in the case that the CSV file
	 * has NO header. In this case the keys of the returning map will be the named as "column1", "column2", etc.
	 * @throws IOException 
	 */
	@Test
	public void testCSVReaderConvertNoHeader() throws IOException {
		ServiceChecker<CSVReader> checker = createStaticTrackedChecker(CSVReader.class);
		checker.assertCreations(1, true);
		
		CSVReader service = checker.getTrackedService();
		assertNotNull(service);
		
		String filePath = "data/2019-05-12T13-31-04_NOHeader.csv";
		Map<String, List<Object>> result = service.convert(filePath, ",", false);
		assertTrue(result.containsKey("column0"));
		assertTrue(result.containsKey("column1"));
		assertTrue(result.containsKey("column2"));
		assertTrue(result.containsKey("column3"));
		
		assertEquals("1557660680836", result.get("column0").get(0));
		assertEquals("0", result.get("column1").get(0));
		assertEquals("50.8583553497009", result.get("column2").get(0));
		assertEquals("12.162946570149", result.get("column3").get(0));
	}
	
	/**
	 * Tests the implementation of the CSVReader when the data types are passed as options.
	 * In this case the data should be saved with the corresponding data type.
	 * An Header in the CSV file is also assumed for this test.
	 * @throws IOException
	 */
	@Test
	public void testCSVReaderConvertHeaderOptions() throws IOException {
		
		ServiceChecker<CSVReader> checker = createStaticTrackedChecker(CSVReader.class);
		checker.assertCreations(1, true);
		
		CSVReader service = checker.getTrackedService();
		assertNotNull(service);
		
		String filePath = "data/2019-05-12T13-31-04.csv";
		
		Map<String, Object> options = CSVReader.createDefaultOptions(",", true);
		Class<?>[] types = {Long.TYPE, Integer.TYPE, String.class, String.class};
		options.put(CSVReader.PROP_TYPE_ARRAY, types);
		
		Map<String, List<Object>> result = service.convert(filePath, options);
		assertTrue(result.containsKey("timestamp"));
		assertTrue(result.containsKey("speed in km/h"));
		assertTrue(result.containsKey("latitude"));
		assertTrue(result.containsKey("longitude"));
		
		assertTrue(result.get("timestamp").get(0) instanceof Long);
		assertTrue(result.get("speed in km/h").get(0) instanceof Integer);
		assertTrue(result.get("latitude").get(0) instanceof String);
		assertTrue(result.get("longitude").get(0) instanceof String);
	}
	
	/**
	 * Tests the implementation of the CSVReader when the data types are passed as options.
	 * In this case the data should be saved with the corresponding data type.
	 * NO Header in the CSV file is assumed for this test.
	 * @throws IOException
	 */
	@Test
	public void testCSVReaderConvertNOHeaderOptions() throws IOException {
		
		ServiceChecker<CSVReader> checker = createStaticTrackedChecker(CSVReader.class);
		checker.assertCreations(1, true);
		
		CSVReader service = checker.getTrackedService();
		assertNotNull(service);
		
		String filePath = "data/2019-05-12T13-31-04_NOHeader.csv";
		
		Map<String, Object> options = CSVReader.createDefaultOptions(",", false);
		Class<?>[] types = {Long.TYPE, Integer.TYPE, String.class, String.class};
		options.put(CSVReader.PROP_TYPE_ARRAY, types);
		
		Map<String, List<Object>> result = service.convert(filePath, options);
		assertTrue(result.containsKey("column0"));
		assertTrue(result.containsKey("column1"));
		assertTrue(result.containsKey("column2"));
		assertTrue(result.containsKey("column3"));
		
		assertTrue(result.get("column0").get(0) instanceof Long);
		assertTrue(result.get("column1").get(0) instanceof Integer);
		assertTrue(result.get("column2").get(0) instanceof String);
		assertTrue(result.get("column3").get(0) instanceof String);
	}
	
	/**
	 * Check the CSVReader convert method when passing an InputStream instead of a file as parameter.
	 * An header is also assumed for the file.
	 * @throws IOException
	 */
	@Test
	public void testCSVReaderInputStreamConvertHeaderOptions() throws IOException {
		
		ServiceChecker<CSVReader> checker = createStaticTrackedChecker(CSVReader.class);
		checker.assertCreations(1, true);
		
		CSVReader service = checker.getTrackedService();
		assertNotNull(service);
		
		String filePath = "data/2019-05-12T13-31-04.csv";
		InputStream stream = new FileInputStream(filePath);
		
		Map<String, Object> options = CSVReader.createDefaultOptions(",", true);
		Class<?>[] types = {Long.TYPE, Integer.TYPE, String.class, String.class};
		options.put(CSVReader.PROP_TYPE_ARRAY, types);
		
		Map<String, List<Object>> result = service.convert(stream, options);
		assertTrue(result.containsKey("timestamp"));
		assertTrue(result.containsKey("speed in km/h"));
		assertTrue(result.containsKey("latitude"));
		assertTrue(result.containsKey("longitude"));
		
		assertTrue(result.get("timestamp").get(0) instanceof Long);
		assertTrue(result.get("speed in km/h").get(0) instanceof Integer);
		assertTrue(result.get("latitude").get(0) instanceof String);
		assertTrue(result.get("longitude").get(0) instanceof String);
	}
	
	/**
	 * Tests the implementation of the CSVReader when an InputStream is passed instead of a File
	 * In this case the data should be saved with the corresponding data type.
	 * NO Header in the CSV file is assumed for this test.
	 * @throws IOException
	 */
	@Test
	public void testCSVReaderInputStreamConvertNOHeaderOptions() throws IOException {
		
		ServiceChecker<CSVReader> checker = createStaticTrackedChecker(CSVReader.class);
		checker.assertCreations(1, true);
		
		CSVReader service = checker.getTrackedService();
		assertNotNull(service);
		
		String filePath = "data/2019-05-12T13-31-04_NOHeader.csv";
		InputStream stream = new FileInputStream(filePath);
		
		Map<String, Object> options = CSVReader.createDefaultOptions(",", false);
		Class<?>[] types = {Long.TYPE, Integer.TYPE, String.class, String.class};
		options.put(CSVReader.PROP_TYPE_ARRAY, types);
		
		Map<String, List<Object>> result = service.convert(stream, options);
		assertTrue(result.containsKey("column0"));
		assertTrue(result.containsKey("column1"));
		assertTrue(result.containsKey("column2"));
		assertTrue(result.containsKey("column3"));
		
		assertTrue(result.get("column0").get(0) instanceof Long);
		assertTrue(result.get("column1").get(0) instanceof Integer);
		assertTrue(result.get("column2").get(0) instanceof String);
		assertTrue(result.get("column3").get(0) instanceof String);
	}
	
}

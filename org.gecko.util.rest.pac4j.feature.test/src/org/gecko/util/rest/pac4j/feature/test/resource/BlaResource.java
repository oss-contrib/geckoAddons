/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.rest.pac4j.feature.test.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gecko.util.rest.pac4j.feature.test.annotation.RequireRuntime;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.jaxrs.runtime.JaxrsServiceRuntime;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsApplicationSelect;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsName;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;

/**
 * 
 * @author jalbert
 * @since 31 Aug 2018
 */
@RequireRuntime
@Component(service = BlaResource.class, scope = ServiceScope.PROTOTYPE)
@JaxrsResource
@JaxrsName("BlaResource")
@JaxrsApplicationSelect("(osgi.jaxrs.name=*)")
@Consumes(MediaType.WILDCARD)
@Produces(MediaType.WILDCARD)
@Path("bla")
public class BlaResource {
	
	@Reference
	List<JaxrsServiceRuntime> runtimes;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("test")
	public Response callback () {
		StringBuffer sb = new StringBuffer("["); 
		runtimes.stream().map(runtime -> runtime.getRuntimeDTO()).forEach(dto -> {
			if(sb.length() > 1) {
				sb.append(",");
			}
			sb.append(dto.toString());
		});
		sb.append("]");
		return Response.ok(sb.toString()).build();
	}

}

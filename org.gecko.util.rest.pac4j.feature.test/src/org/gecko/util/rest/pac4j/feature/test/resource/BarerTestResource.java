/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.rest.pac4j.feature.test.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gecko.util.rest.pac4j.feature.test.annotation.RequireRuntime;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsWhiteboardTarget;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.jax.rs.annotations.Pac4JProfile;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;

/**
 * 
 * @author jalbert
 * @since 31 Aug 2018
 */
@RequireRuntime
@Component(service = BarerTestResource.class, scope = ServiceScope.PROTOTYPE)
@JaxrsResource
@JaxrsWhiteboardTarget("(jersey.jaxrs.whiteboard.name=bearer)")
@Consumes(MediaType.WILDCARD)
@Produces(MediaType.WILDCARD)
@Path("/")
public class BarerTestResource {
	

	@GET
	@Path("remote")
	@Pac4JSecurity(clients = "bearer")
	public Response login(@Pac4JProfile CommonProfile profile) {
		return Response.ok("Returend " + profile.getFirstName() + " " + profile.getFamilyName()).build();
	}
}

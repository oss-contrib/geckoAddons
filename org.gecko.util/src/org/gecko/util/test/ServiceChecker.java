/**
 * Copyright (c) 2012 - 2016 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.test;

import static org.junit.Assert.fail;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Helper class that helps waiting for services to create, update or delete.
 * It adds support for blocking using {@link CountDownLatch}. Internally it uses the service tracker to
 * Track services
 * @author Mark Hoffmann
 * @param <T>
 * @since 10.11.2016
 */
public class ServiceChecker<T> {
	
	private final static Logger logger = Logger.getLogger(ServiceChecker.class.getName());
	
	// default create expectation count
	private int createExpectationCount = 1;
	private int removalExpectationCount = 1;
	private int modifyExpectationCount = 1;
	// timeouts in seconds
	private int createTimeout = 5;
	private int modifyTimeout = 5;
	private int removalTimeout = 5;
	// latches
	private CountDownLatch createLatch = null;
	private CountDownLatch modifyLatch = null;
	private CountDownLatch removeLatch = null;
	
	final private BundleContext context;
	private ServiceTracker<T, T> tracker;
	private ServiceProviderCustomizer<T, T> customizer = null;
	private Class<T> serviceClass = null;
	private Filter filter = null;
	private boolean running = false;
	
	/**
	 * Returns the expectation count for create
	 * @return the expectation count for create
	 */
	public int getCreateExpectationCount() {
		return createExpectationCount;
	}

	/**
	 * Sets the expectation count for service creations. 1 or 0 are common values
	 * @param createExpected the value that will be expected
	 */
	public void setCreateExpectationCount(int createExpected) {
		if (createExpected < 0) {
			logger.log(Level.WARNING, "An expection of {0} service creations is not valid and wont be set", createExpected);
			return;
		}
		if (createExpected > 1) {
			logger.log(Level.WARNING, "An expection of {0} service creations was given, 0 or 1 would make sense", createExpected);
		}
		this.createExpectationCount = createExpected;
	}

	/**
	 * Returns the expectation count for service modification
	 * @return the expectation count for service modification
	 */
	public int getModifyExpectationCount() {
		return modifyExpectationCount;
	}

	/**
	 * Sets the expectation count for service modification. 
	 * @param createExpected the value that will be expected
	 */
	public void setModifyExpectationCount(int modifyExpected) {
		if (modifyExpected < 0) {
			logger.log(Level.WARNING, "An expection of {0} service modification is not valid and wont be set", modifyExpected);
			return;
		}
		this.modifyExpectationCount = modifyExpected;
	}

	/**
	 * Returns the expectation count for service removal
	 * @return the expectation count for service removal
	 */
	public int getRemovalExpectationCount() {
		return removalExpectationCount;
	}

	/**
	 * Sets the expectation count for service removals. 1 or 0 are common values
	 * @param removeExpected the value that will be expected
	 */
	public void setRemovalExpectationCount(int removeExpected) {
		if (removeExpected < 0) {
			logger.log(Level.WARNING, "An expection of {0} service removals is not valid and wont be set", removeExpected);
			return;
		}
		if (removeExpected > 1) {
			logger.log(Level.WARNING, "An expection of {0} service removals was given, 0 or 1 would make sense", removeExpected);
		}
		this.removalExpectationCount = removeExpected;
	}

	/**
	 * Returns the value for the timeout of a service creation
	 * @return the value for the timeout of a service creation
	 */
	public int getCreateTimeout() {
		return createTimeout;
	}

	/**
	 * Sets the timeout in seconds for a service creation
	 * @param createTimeout the value to be set
	 */
	public void setCreateTimeout(int createTimeout) {
		this.createTimeout = createTimeout;
	}

	/**
	 * Returns the value for the timeout of a service modification
	 * @return the value for the timeout of a service modification
	 */
	public int getModifyTimeout() {
		return modifyTimeout;
	}

	/**
	 * Sets the timeout in seconds for a service modification
	 * @param modifyTimeout the value to be set
	 */
	public void setModifyTimeout(int modifyTimeout) {
		this.modifyTimeout = modifyTimeout;
	}

	/**
	 * Returns the value for the timeout of a service removal
	 * @return the value for the timeout of a service removal
	 */
	public int getRemoveTimeout() {
		return removalTimeout;
	}

	/**
	 * Sets the timeout in seconds for a service removal
	 * @param removalTimeout the value to be set
	 */
	public void setRemovalTimeout(int removalTimeout) {
		this.removalTimeout = removalTimeout;
	}

	/**
	 * Creates a new instance.
	 * @param serviceClass the class to be tracked
	 * @param context the bundle context
	 */
	public ServiceChecker(Class<T> serviceClass, BundleContext context) {
		this.serviceClass = serviceClass;
		this.context = context;
	}
	
	/**
	 * Creates a new instance.
	 * @param filter the filter as string
	 * @param context the bundle context
	 * @throws InvalidSyntaxException
	 */
	public ServiceChecker(String filter, BundleContext context) throws InvalidSyntaxException {
		this.filter = context.createFilter(filter);
		this.context = context;
	}
	
	/**
	 * Creates a new instance.
	 * @param filter the filter 
	 * @param context the bundle context
	 */
	public ServiceChecker(Filter filter, BundleContext context) {
		this.filter = filter;
		this.context = context;
	}
	
	/**
	 * Starts the checker
	 */
	public void start() {
		if (context == null || (serviceClass == null && filter == null)) {
			fail("Error starting checker for service because service class or filter or bundle context is/are null");
		}
		if (running) {
			fail("Service check is already running");
		} else {
			running = true;
		}
		createLatch = new CountDownLatch(getCreateExpectationCount());
		modifyLatch = new CountDownLatch(getModifyExpectationCount());
		removeLatch = new CountDownLatch(getRemovalExpectationCount());
		
		customizer = new ServiceProviderCustomizer<T, T>(context, createLatch, removeLatch, modifyLatch);
		if(serviceClass != null) {
			tracker = new ServiceTracker<T, T>(context, serviceClass, customizer);
		} else {
			tracker = new ServiceTracker<T, T>(context, filter, customizer);
		}
		tracker.open(true);
	}

	/**
	 * Stops the service checker
	 */
	public void stop() {
		if (tracker != null) {
			tracker.close();
			tracker = null;
		}
		if (customizer != null) {
			customizer = null;
		}
		createLatch = null;
		modifyLatch = null;
		removeLatch = null;
		if (!running) {
			fail("Cannot stop check, because it is not running");
		} else {
			running = false;
		}
	}
	
	/**
	 * Returns the tracked service, that maybe <code>null</code>. It waits max 1s for the service to come
	 * @return the tracked service, that maybe <code>null</code>
	 */
	public T getTrackedService() {
		failIfNotRunning("Getting service");
		try {
			return tracker.waitForService(1000l);
		} catch (InterruptedException e) {
			throw new IllegalStateException(e);
		}
	}
	
	/**
	 * Returns the tracked service reference, that maybe <code>null</code>
	 * @return the tracked service reference, that maybe <code>null</code>
	 */
	public ServiceReference<T> getTrackedServiceReference() {
		failIfNotRunning("Getting service reference");
		try {
			ServiceReference<T> ref = null;
			long timeout = 1000;
			long timeToWait = 5;
			do {
				Thread.sleep(timeToWait);
				timeout -= timeToWait;
				ref = tracker.getServiceReference();
				if (timeout <= 0) {
					break;
				}
			} while (ref == null);
			return ref;
		} catch (InterruptedException e) {
			throw new IllegalStateException(e);
		}
	}
	
	/**
	 * Awaits the service creation to the given timeout
	 * @return <code>true</code>, if finished
	 * @throws InterruptedException
	 */
	public boolean awaitCreation() throws InterruptedException {
		failIfNotRunning("Await service creation");
		return createLatch.await(createTimeout, TimeUnit.SECONDS);
	}
	
	/**
	 * Awaits the service modification to the given timeout
	 * @return <code>true</code>, if finished
	 * @throws InterruptedException
	 */
	public boolean awaitModification() throws InterruptedException {
		failIfNotRunning("Await service modification");
		return modifyLatch.await(modifyTimeout, TimeUnit.SECONDS);
	}
	
	/**
	 * Awaits the service removal to the given timeout
	 * @return <code>true</code>, if finished
	 * @throws InterruptedException
	 */
	public boolean awaitRemoval() throws InterruptedException {
		failIfNotRunning("Await service removal");
		return removeLatch.await(removalTimeout, TimeUnit.SECONDS);
	}
	
	/**
	 * Returns the current count, how many times the create of a service was triggered
	 * @param wait <code>true</code>, if wait till the given create timeout, default is 5 seconds
	 * @return the number of service creations
	 */
	public int getCurrentCreateCount(boolean wait) {
		failIfNotRunning("Current creation count");
		if (customizer != null) {
			if (wait) {
				try {
					awaitCreation();
				} catch (InterruptedException e) {
					fail("CurrentCreationCount was interrupted");
				}
			}
			return customizer.getAddCount();
		} else {
			throw new IllegalStateException("No customizer was created. This must be an error.");
		}
	}

	/**
	 * Returns the current count, how many times the modification of a service was triggered
	 * @param wait <code>true</code>, if wait till the given create timeout, default is 5 seconds
	 * @return the number of service modifications
	 */
	public int getCurrentModifyCount(boolean wait) {
		failIfNotRunning("Current modification count");
		if (customizer != null) {
			if (wait) {
				try {
					awaitModification();
				} catch (InterruptedException e) {
					fail("CurrentModificationCount was interrupted");
				}
			}
			return customizer.getModifyCount();
		} else {
			throw new IllegalStateException("No customizer was created. This must be an error.");
		}
	}
	
	/**
	 * Returns the current count, how many times the removal of a service was triggered
	 * @param wait <code>true</code>, if wait till the given create timeout, default is 5 seconds
	 * @return the number of service removals
	 */
	public int getCurrentRemoveCount(boolean wait) {
		failIfNotRunning("Current removal count");
		if (customizer != null) {
			if (wait) {
				try {
					awaitRemoval();
				} catch (InterruptedException e) {
					fail("CurrentRemoveCount was interrupted");
				}
			}
			return customizer.getRemoveCount();
		} else {
			throw new IllegalStateException("No customizer was created. This must be an error.");
		}
	}
	
	/**
	 * This method checks for state running and provides the given message
	 * @param message the fail message
	 */
	private void failIfNotRunning(String message) {
		if (!running) {
			fail("Checker is not running. Did you call start? " + message);
		}
	}

}

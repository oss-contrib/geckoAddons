/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceObjects;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.tracker.ServiceTracker;

/**
 * A basic test for OSGi. It provides convenient access to services and manages cleanup operations,
 * when services are registered via the internal methods. 
 * @author Juergen Albert
 * @since 30 Jul 2018
 */
public abstract class AbstractOSGiTest {

	private final BundleContext bundleContext;

	Map<Object,ServiceReference<?>> referencesToCleanup = new HashMap<>();
	Map<ServiceRegistration<?>, Object> registrationsToCleanup = new HashMap<>();
	Map<Object, ServiceTracker<?, ?>> serviceTrackerToCleanUp = new HashMap<>();

	Map<Configuration, ServiceChecker<?>> configsToClean = new HashMap<>();
	List<ServiceChecker<?>> waitForRemoves = new LinkedList<>();

	private ConfigurationAdmin configAdmin;

	private boolean removeConfigurationTrackServices = true;

	/**
	 * Creates a new instance.
	 */
	public AbstractOSGiTest(BundleContext bundleContext) {
		this.bundleContext = bundleContext;
	}

	@Before
	public void before() {
		configAdmin = getService(ConfigurationAdmin.class);
		doBefore();
	}

	/**
	 * will be called in the do before, after the Abstract tests has initialized
	 */
	public abstract void doBefore();

	/**
	 * Returns <code>true</code>, if service are tracked for removal when cleaning up configurations.
	 * @return the removeConfigurationTrackServices
	 */
	public boolean isRemoveConfigurationTrackServices() {
		return removeConfigurationTrackServices;
	}

	/**
	 * Enables or disables tracking of services when removing configurations. As default this is activated.
	 * When activated each time a configuration is removed, a corresponding service is tracked for removal.
	 * @param track the value to set
	 */
	public void setRemoveConfigurationTrackServices(boolean track) {
		removeConfigurationTrackServices = track;
	}
	/**
	 * Cleanly ungets the given service, if it was retrieved via the getService method.
	 * @param service the service to unget
	 */
	protected void ungetService(Object service) {
		ServiceReference<?> toUnget = referencesToCleanup.remove(service);
		if(toUnget != null) {
			getBundleContext().ungetService(toUnget);
		} else {
			ServiceTracker<?,?> serviceTracker = serviceTrackerToCleanUp.remove(service);
			if(serviceTracker != null) {
				serviceTracker.close();
			}
		}
	}

	/**
	 * This method will retrieve a Service and does a {@link Assert#assertNotNull(Object)} check. When the test ends, the service will be unget as well. 
	 * @param clazz The Service Class desired
	 * @return the {@link ServiceReference}
	 */
	protected <T> ServiceReference<T> getServiceReference(Class<T> clazz) {
		ServiceReference<T> serviceReference = bundleContext.getServiceReference(clazz);
		assertNotNull(serviceReference);
		T service = bundleContext.getService(serviceReference);
		referencesToCleanup.put(service, serviceReference);
		return serviceReference;
	}

	/**
	 * This method will retrieve a Service and does a {@link Assert#assertNotNull(Object)} check. When the test ends, the service will be unget as well. 
	 * @param clazz The Service Class desired
	 * @return the {@link ServiceObjects}
	 */
	protected <T> ServiceObjects<T> getServiceObjects(Class<T> clazz) {
		ServiceReference<T> serviceReference = bundleContext.getServiceReference(clazz);
		assertNotNull(serviceReference);
		ServiceObjects<T> serviceObjects = bundleContext.getServiceObjects(serviceReference);
		referencesToCleanup.put(serviceObjects, serviceReference);
		return serviceObjects;
	}

	/**
	 * This method will retrieve a Service and does a {@link Assert#assertNotNull(Object)} check. When the test ends, the service will be unget as well. 
	 * @param clazz The Service Class desired
	 * @param filter the filter to apply as well
	 * @return the {@link ServiceObjects}
	 * @throws InvalidSyntaxException 
	 */
	protected <T> ServiceObjects<T> getServiceObjects(Class<T> clazz, String filter) throws InvalidSyntaxException {
		Collection<ServiceReference<T>> serviceReferences = bundleContext.getServiceReferences(clazz, filter);
		assertNotNull(serviceReferences);
		assertFalse("No Service Reference found for " + clazz.getName() + " and Filter " + filter, serviceReferences.isEmpty());
		ServiceReference<T> next = serviceReferences.iterator().next();
		ServiceObjects<T> serviceObjects = bundleContext.getServiceObjects(next);
		referencesToCleanup.put(serviceObjects, next);
		return serviceObjects;
	}

	/**
	 * This method will retrieve a Service and does a {@link Assert#assertNotNull(Object)} check. When the test ends, the service will be unget as well. 
	 * @param clazz The Service Class desired
	 * @return the service will never be null 
	 */
	protected <T> T getService (Class<T> clazz) {
		ServiceReference<T> serviceReference = bundleContext.getServiceReference(clazz);
		assertNotNull(serviceReference);
		T service = bundleContext.getService(serviceReference);
		referencesToCleanup.put(service, serviceReference);
		return service;
	}

	/**
	 * This method will retrieve a Service and does a {@link Assert#assertNotNull(Object)} check. When the test ends, the service will be unget as well. 
	 * @param filter the {@link Filter} to use 
	 * @return the service, will never be null
	 * @throws InterruptedException 
	 */
	protected <T> T getService (Filter filter, long timeout) throws InterruptedException {
		ServiceTracker<T, T> tracker = new ServiceTracker<>(getBundleContext(), filter, null);
		tracker.open();
		T service = tracker.waitForService(timeout);
		serviceTrackerToCleanUp.put(service, tracker);
		assertNotNull(service);
		return service;
	}

	/**
	 * This method will try to a Service and does a {@link Assert#assertNull(Object)} check. 
	 * @param filter the {@link Filter} to use 
	 * @throws InterruptedException 
	 */
	protected void getServiceAssertNull(Filter filter) throws InterruptedException {
		ServiceTracker<?, ?> tracker = new ServiceTracker<>(getBundleContext(), filter, null);
		tracker.open();
		Object service = tracker.waitForService(50);
		serviceTrackerToCleanUp.put(new Object(), tracker);
		assertNull(service);
	}

	/**
	 * A configuration is created and update will be called. The config is furthermore stored for later cleanup.
	 * Note that the cleanup expects a Service that can be tracked on cleanup. 
	 * @param factoryPid the factoryPid of the service
	 * @param location the location of the configuration
	 * @param props the properties for the service
	 * @return the config you desire
	 * @throws IOException
	 */
	protected Configuration createConfigForCleanup(String factoryPid, String location, Dictionary<String, Object> props) throws IOException {
		Configuration config = configAdmin.createFactoryConfiguration(factoryPid, "?");
		Filter filter = createFilter(props);
		ServiceChecker<?> checker = new ServiceChecker<>(filter, getBundleContext());
		configsToClean.put(config, checker);
		checker.start();
		config.update(props);
		return config;
	}

	/**
	 * A configuration is created and update will be called. The config is furthermore stored for later cleanup.
	 * Note that the cleanup expects a Service that can be tracked on cleanup. 
	 * @param factoryPid the factoryPid of the service
	 * @param name the addition configuration name of the service
	 * @param location the location of the configuration
	 * @param props the properties for the service
	 * @return the config you desire
	 * @throws IOException
	 */
	protected Configuration createConfigForCleanup(String factoryPid, String name, String location, Dictionary<String, Object> props) throws IOException {
		Configuration config = configAdmin.getFactoryConfiguration(factoryPid, name, "?");
		Filter filter = createFilter(props);
		ServiceChecker<?> checker = new ServiceChecker<>(filter, getBundleContext());
		configsToClean.put(config, checker);
		checker.start();
		config.update(props);
		return config;
	}
	
	/**
	 * Returns a service checker for the configuration
	 * @param configuration the configuration to get a tracker for
	 * @return the {@link ServiceChecker} or <code>null</code>
	 */
	protected ServiceChecker<?> getServiceCheckerForConfiguration(Configuration configuration) {
		if (configuration == null) {
			return null;
		}
		return configsToClean.get(configuration);
	}

	/**
	 * The {@link Configuration} to delete
	 * @param config the {@link Configuration} to remove
	 * @throws IOException
	 */
	protected void deleteConfigurationAndRemoveFromCleanup(Configuration config) throws IOException {
		configsToClean.remove(config);
		config.delete();
	}

	/**
	 * The {@link Configuration} to delete. The method will block, until it receives the notification, that the service is removed. 
	 * The method will throw a {@link RuntimeException} after 5 seconds if nothing happens.
	 * @param config the {@link Configuration} to remove
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	protected void deleteConfigurationAndRemoveFromCleanupBlocking(Configuration config) throws IOException, InterruptedException {
		configsToClean.remove(config);
		Filter filter = createFilter(config.getProperties()); 
		ServiceChecker<?> checker = new ServiceChecker<>(filter, getBundleContext());
		checker.start();
		config.delete();
		if(!checker.awaitRemoval()) {
			fail("Remove configuration - Service remove never appeared.");
		};
		checker.stop();
		return;
	}

	/**
	 * The {@link Configuration} to delete. The method will block, until it receives the notification, that the service is removed. 
	 * The method will throw a {@link RuntimeException} after 5 seconds if nothing happens.
	 * @param config the {@link Configuration} to remove
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	protected void deleteConfigurationBlocking(Configuration config) throws IOException, InterruptedException {
		ServiceChecker<?> checker = configsToClean.remove(config);
		config.delete();
		try {
			if(checker != null && 
					isRemoveConfigurationTrackServices() && 
					!checker.awaitRemoval()) {
				fail("Remove configuration blocking - Service remove never appeared.");
			}
		} finally {
			if (checker != null) {
				checker.stop();
			}
		}
		return;
	}

	/**
	 * Registers the given service Object. This serivce will re unregistered, when the test is teared down.
	 * Use {@link AbstractOSGiTest#unregisterService(Object)} to unregister your service.
	 * @param service the Service {@link Object} to register 
	 * @param properties the properties connected with this service
	 * @param clazz the clazz to register the service under
	 */
	protected void registerServiceForCleanup(Object service, Dictionary<String, ?> properties, Class<?> clazz) {
		registerServiceForCleanup(service, properties, clazz.getName());
	}

	/**
	 * Registers the given service Object. This serivce will re unregistered, when the test is teared down.
	 * Use {@link AbstractOSGiTest#unregisterService(Object)} to unregister your service.
	 * @param service the Service {@link Object} to register 
	 * @param properties the properties connected with this service
	 * @param clazz the clazz to register the service under
	 */
	protected void registerServiceForCleanup(Class<?> clazz, Object service, Dictionary<String, ?> properties) {
		registerServiceForCleanup(service, properties, clazz.getName());
	}

	/**
	 * Registers the given service Object. This serivce will re unregistered, when the test is teared down.
	 * Use {@link AbstractOSGiTest#unregisterService(Object)} to unregister your service.
	 * @param service the Service {@link Object} to register 
	 * @param properties the properties connected with this service
	 * @param classes the classes to register the service under
	 */
	protected void registerServiceForCleanup(Object service, Dictionary<String, ?> properties, String... classes) {
		assertNotNull(service);
		assertNotNull(properties);
		assertNotEquals(0, classes.length);
		ServiceRegistration<?> registration = getBundleContext().registerService(classes, service, properties);
		registrationsToCleanup.put(registration, service);
	}

	/**
	 * Updates the given Service with the given Properties
	 * @param service the service to update
	 * @param properties the properties to use
	 */
	public void updateServiceRegistration(Object service, Dictionary<String, ?> properties) {
		assertNotNull(service);
		assertNotNull(properties);
		ServiceRegistration<?> registration = registrationsToCleanup.entrySet().stream().filter(e->e.getValue().equals(service)).map(Map.Entry::getKey).findFirst().orElse(null);
		assertNotNull(registration);
		registration.setProperties(properties);
	}

	/**
	 * returns the service properties of the {@link ServiceRegistration} for the given Service
	 * @param service the service to look for
	 * @return the properties of the current service reference
	 */
	public Dictionary<String,Object> getServiceProperties(Object service) {
		assertNotNull(service);
		ServiceRegistration<?> registration = registrationsToCleanup.entrySet().stream().filter(e->e.getValue().equals(service)).map(Map.Entry::getKey).findFirst().orElse(null);
		assertNotNull(registration);
		Dictionary<String, Object> props = new Hashtable<>();
		ServiceReference<?> reference = registration.getReference();
		for(String key : reference.getPropertyKeys()) {
			props.put(key, reference.getProperty(key));
		}
		return props;
	}

	/**
	 * Asserts that the given Object is not null and that we know about a {@link ServiceRegistration}. The service will then be unregistered.
	 * @param service the service to unregister.
	 */
	protected void unregisterService(Object service) {
		assertNotNull(service);
		ServiceRegistration<?> remove = registrationsToCleanup.entrySet().stream().filter(e->e.getValue().equals(service)).map(Map.Entry::getKey).findFirst().orElse(null);
		assertNotNull(remove);
		registrationsToCleanup.remove(remove);
		remove.unregister();
	}


	/**
	 * Creates a filter String looking for all properties in the {@link Dictionary} given.
	 * @param properties the {@link Dictionary} with the service properties.
	 * @return the {@link Filter} Object
	 */
	protected Filter createFilter(Dictionary<String, Object> properties){
		StringBuilder sb = new StringBuilder("(&");
		Enumeration<String> keys = properties.keys();
		while(keys.hasMoreElements()) {
			String key = keys.nextElement();
			Object value = properties.get(key);
			sb.append(String.format("(%s=%s)", key, value.toString()));
		}
		sb.append(")");
		try {
			return FrameworkUtil.createFilter(sb.toString());
		} catch (InvalidSyntaxException e) {
			throw new IllegalArgumentException("Could not create filter " + e.getFilter(), e);
		}
	}

	/**
	 * Creates a ServiceChecker for a specific Service, where at least one Remove is expected. 
	 * At the End of the Test, it will block until the service is definitely gone.
	 * @param filter the OSGi filter String to create the checker with
	 * @return the {@link ServiceChecker}
	 * @throws InvalidSyntaxException if the filter syntax is wrong
	 */
	protected <T extends Object> ServiceChecker<T> createCheckerTrackedForCleanUp(Class<T> serviceClass) {
		ServiceChecker<T> checker = new ServiceChecker<>(serviceClass, getBundleContext());
		waitForRemoves.add(checker);
		return (ServiceChecker<T>) checker;
	}

	/**
	 * Creates a ServiceChecker for a specific Service, where at least one Remove is expected. 
	 * At the End of the Test, it will block until the service is definitely gone.
	 * @param filter the OSGi filter String to create the checker with
	 * @return the {@link ServiceChecker}
	 * @throws InvalidSyntaxException if the filter syntax is wrong
	 */
	protected <T extends Object> ServiceChecker<T>  createdCheckerTrackedForCleanUp(String filter) throws InvalidSyntaxException {
		ServiceChecker<T> checker = new ServiceChecker<>(filter, getBundleContext());
		waitForRemoves.add(checker);
		return (ServiceChecker<T>) checker;
	}

	/**
	 * Cleanup Method
	 * @throws Exception
	 */
	@After
	public void after() throws Exception {
		try {
			doAfter();
		} catch (Exception e) {
			
		}
		try {
			Collection<Configuration> configs = new ArrayList<Configuration>(configsToClean.keySet());
			configs.forEach(c -> {
				try {
					deleteConfigurationBlocking(c);
				} catch (Exception e) {
					fail(e.getMessage());
				}
			});
		} catch (Exception e) {
			fail(e.getMessage());
		}

		configAdmin = null;
		referencesToCleanup.forEach((o, s) -> bundleContext.ungetService(s));
		referencesToCleanup.clear();
		registrationsToCleanup.forEach((s, o) -> s.unregister());
		registrationsToCleanup.clear();

		serviceTrackerToCleanUp.forEach((o, s) -> s.close());
		serviceTrackerToCleanUp.clear();

		waitForRemoves.forEach(sc -> {
			try {
				sc.awaitRemoval();
			} catch (InterruptedException e) {
				assertNull(e);
			} finally {
				sc.stop();
			}
		});
		
	}

	/**
	 * will be called in the before the test performs its cleanup, after the Abstract tests has initialized
	 */
	public abstract void doAfter();

	/**
	 * Returns the configAdmin.
	 * @return the configAdmin
	 */
	public ConfigurationAdmin getConfigAdmin() {
		return configAdmin;
	}

	/**
	 * Returns the bundleContext.
	 * @return the bundleContext
	 */
	public BundleContext getBundleContext() {
		return bundleContext;
	}

}

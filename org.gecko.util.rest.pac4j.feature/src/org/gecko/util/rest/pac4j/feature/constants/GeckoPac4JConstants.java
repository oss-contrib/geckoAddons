/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.rest.pac4j.feature.constants;

/**
 * 
 * @author Juergen Albert
 * @since 9 Sep 2018
 */
public class GeckoPac4JConstants {

	public static final String NS = "gecko.jaxrs.security"; 
	public static final String NAME = "pac4j"; 
	
}
